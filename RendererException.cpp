#include	"RendererException.hh"

RendererException::RendererException(const std::string& desc)
  : desc(desc)
{}

RendererException::~RendererException() throw()
{}

const char*	RendererException::what() const throw()
{
  return (this->desc.c_str());
}

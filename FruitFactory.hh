#ifndef FRUITFACTORY_H_
# define FRUITFACTORY_H_

# include	<vector>
# include	"AFruit.hh"

class FruitFactory
{
protected:
  std::vector<std::pair<int, AFruit *> >	fruits;

  FruitFactory(const FruitFactory&);
  FruitFactory&	operator=(const FruitFactory&);

public:
  FruitFactory();
  ~FruitFactory();

  void	addFruit(int prob, AFruit * fruit);
  AFruit *pickFruit() const;

};

#endif /* !FRUITFACTORY_H_ */

#ifndef KIWI_H_
# define KIWI_H_

# include	"AFruit.hh"

class Kiwi : public AFruit
{
protected:
  Kiwi(const Kiwi&);
  Kiwi&	operator=(const Kiwi&);

public:
  Kiwi();
  ~Kiwi();

  void eaten(ASnake &snake) const;
  AFruit *clone() const;
};

#endif /* !KIWI_H_ */

#include	"SnakePlayer.hh"

//----- ----- Constructors ----- ----- //
SnakePlayer::SnakePlayer(std::pair<int, int> coords)
  : ASnake(coords)
{
  this->type = AEntity::SNAKEPLAYER;
}

//----- ----- Destructor ----- ----- //
SnakePlayer::~SnakePlayer()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	SnakePlayer::updateDirection(const Map&, const std::vector<ASnake *>&)
{
}

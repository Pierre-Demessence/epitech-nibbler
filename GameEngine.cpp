#include	<dlfcn.h>
#include	<iostream>
#include	<unistd.h>

#include	"GameEngine.hh"
#include	"RendererException.hh"
#include	"SHome.hh"

//----- ----- Constructors ----- ----- //
GameEngine::GameEngine()
{
  this->stateManager.setGameEngine(this);
  this->rendererIndex = 0;
  this->quit = 0;
}

//----- ----- Destructor ----- ----- //
GameEngine::~GameEngine()
{
  IRenderer*	tmp;
  while (this->renderers.size() != 0)
    {
      tmp = this->renderers.back();
      if (tmp->isReady())
	tmp->pause();
      this->renderers.pop_back();
      delete tmp;
    }
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void	GameEngine::setQuit(bool quit)
{
  this->quit = quit;
}

//----- ----- Methods ----- ----- //
int	GameEngine::update()
{
  return (this->stateManager.update(*(this->renderers[this->rendererIndex])));
}

void	GameEngine::render()
{
  this->stateManager.render(*(this->renderers[this->rendererIndex]));
}

int		GameEngine::loadLibrary(const std::string& path)
{
  void*		handle;
  IRenderer*	(*func)();
  IRenderer*	renderer;

  handle = dlopen(path.c_str(), RTLD_LAZY);
  if (!handle)
    {
      std::cerr << dlerror() << std::endl;
      return (-1);
    }

  *(void **) (&func) = dlsym(handle, "getInstance");
  if (!func)
    {
      std::cerr << dlerror() << std::endl;
      return (-1);
    }

  try
    {
      renderer = dynamic_cast<IRenderer*>((*func)());
      if (!renderer)
	{
	  std::cerr << path << ": Library broken." << std::endl;
	  return (-1);
	}
    }
  catch (const RendererException& e)
    {
      std::cerr << path << ": "<< e.what() << std::endl;
      return (-1);
    }
  for (unsigned int i = 0 ; i < this->renderers.size() ; i++)
    {
      if (renderer->getRendererName() == this->renderers[i]->getRendererName())
	{
	  std::cerr << path << ": Library already added." << std::endl;
	  return (-1);
	}
    }
  this->renderers.push_back(renderer);
  return (0);
}

int	GameEngine::run()
{
  int	sleep_ms;

  if (this->renderers.size() == 0)
    return (E_NORENDER);

  this->stateManager.pushState(new SHome());
  while (!this->quit)
    {
      if (!this->renderers[this->rendererIndex]->isReady())
	{
	  for (unsigned int i = 0 ; i < this->renderers.size() ; i++) {
	    if (i != static_cast<unsigned int>(this->rendererIndex) &&
		this->renderers[i]->isReady())
	      this->renderers[i]->pause();
	  }

	  this->renderers[this->rendererIndex]->init();
	  if (!this->renderers[this->rendererIndex]->isReady())
	    return (E_BADINIT);
	}
      if (this->update() != 0)
	this->quit = 1;
      if (this->renderers[this->rendererIndex]->isReady())
	this->render();
      sleep_ms = 1000.0 / FPS;
      usleep(1000 * sleep_ms);
    }
  return (0);
}


void	GameEngine::nextRenderer()
{
  ++this->rendererIndex;
  if (this->rendererIndex == static_cast<int>(this->renderers.size()))
    this->rendererIndex = 0;
}

void	GameEngine::prevRenderer()
{
  --this->rendererIndex;
  if (this->rendererIndex == -1)
    this->rendererIndex = this->renderers.size() - 1;
}

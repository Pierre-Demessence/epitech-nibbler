#include "Tomato.hh"

//----- ----- Constructors ----- ----- //
Tomato::Tomato() :
  AFruit()
{
  this->type = AEntity::FRUIT;
  this->typeFruit = AFruit::TOMATO;;
}

//----- ----- Destructor ----- ----- //
Tomato::~Tomato()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void   Tomato::eaten(ASnake &snake) const
{
  snake.addMad(42);
}

AFruit *Tomato::clone() const
{
  return (new Tomato());
}

#include "SPause.hh"

//----- ----- Constructors ----- ----- //
SPause::SPause()
{
}

//----- ----- Destructor ----- ----- //
SPause::~SPause()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SPause::isTransparent() const
{
  return (true);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	SPause::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;

  key = renderer.getEvent();

  if (key == IRenderer::SPACE || key == IRenderer::ESCAPE)
    sm.popState(&renderer);
}

void	SPause::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

#ifndef SNAKEAI_H_
# define SNAKEAI_H_

# include	"ASnake.hh"

class	SnakeAI : public ASnake
{
private:
  std::pair<unsigned int, unsigned int> nextPos;
  std::pair<unsigned int, unsigned int> nextRight;
  std::pair<unsigned int, unsigned int> nextLeft;
  std::pair<unsigned int, unsigned int> nextFruit;
  int weight;
  bool something_ahead;
  bool something_right;
  bool something_left;

  SnakeAI(const SnakeAI&);
  SnakeAI& operator=(const SnakeAI&);
  void	getNextPos(std::pair<unsigned int, unsigned int> &);
  void	getNextRight();
  void	getNextLeft();
  void	move(const Map &);

  void calcHardMode(const Map &,
		    const std::pair<unsigned int, unsigned int> &,
		    std::vector<std::pair<unsigned int, unsigned int> >&,
		    unsigned int &);

  char	easyLevelAI();
  char	mediumLevelAI(const Map&);
  char	hardLevelAI(const Map&);
  void	blockMap(const Map&, const std::vector<ASnake *>&);
  void	getNextFruit(const Map&);

public:
  SnakeAI(std::pair<unsigned int, unsigned int> coords);
  ~SnakeAI();

  void	updateDirection(const Map&, const std::vector<ASnake *>&);

  static char	difficultyAI;
};

#endif /* !SNAKEAI_H_ */

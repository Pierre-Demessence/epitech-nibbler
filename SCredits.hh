#ifndef SCREDITS_H_
# define SCREDITS_H_

# include	"IState.hh"

class		SCredits : public IState
{
private:
  SCredits(const SCredits&);
  SCredits&	operator=(const SCredits&);

public:
  SCredits();
  ~SCredits();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  bool	isTransparent() const;
};

#endif /* !SCREDITS_H_ */

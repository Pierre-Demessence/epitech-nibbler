#ifndef STATEMANAGER_H_
# define STATEMANAGER_H_

# include	<cstddef>
# include	<vector>

# include	"IRenderer.hh"
# include	"IState.hh"

class		GameEngine;
class		IState;

class		StateManager
{
private:
  std::vector<IState*>	states;
  GameEngine*		ge;

  StateManager(const StateManager&);
  StateManager&	operator=(const StateManager&);

public:
  StateManager();
  ~StateManager();

  void	popState(IRenderer* = NULL);
  void	pushState(IState*, IRenderer* = NULL);
  int	update(IRenderer&);
  void	render(IRenderer&);
  void	nextRenderer();
  void	prevRenderer();
  void	setGameEngine(GameEngine* ge);
};

#endif /* !STATEMANAGER_H_ */

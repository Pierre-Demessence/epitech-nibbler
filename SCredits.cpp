#include "SCredits.hh"

//----- ----- Constructors ----- ----- //
SCredits::SCredits()
{
}

//----- ----- Destructor ----- ----- //
SCredits::~SCredits()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SCredits::isTransparent() const
{
  return (false);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	SCredits::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;

  key = renderer.getEvent();

  if (key != IRenderer::NONE)
    sm.popState(&renderer);
}

void	SCredits::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

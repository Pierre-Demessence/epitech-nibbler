#include "Wall.hh"

//----- ----- Constructors ----- ----- //
Wall::Wall(std::pair<int, int> coords)
  : AEntity(coords)
{
  this->type = AEntity::WALL;
}

//----- ----- Destructor ----- ----- //
Wall::~Wall()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	Wall::update()
{}

#ifndef SHOME_H_
# define SHOME_H_

# include	<string>
# include	<vector>

# include	"IState.hh"
# include	"SCredits.hh"

class		SHome : public IState
{
private:
  std::vector<std::string>	menuEntries;
  std::vector<void (SHome::*)(IRenderer&, StateManager&)>	menuActions;
  int				selectedEntry;

  SHome(const SHome&);
  SHome&	operator=(const SHome&);

  void		menuPlay(IRenderer& renderer, StateManager& sm);
  void		menuOptions(IRenderer& renderer, StateManager& sm);
  void		menuCredits(IRenderer& renderer, StateManager& sm);
  void		menuQuit(IRenderer& renderer, StateManager& sm);

public:
  SHome();
  ~SHome();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  bool	isTransparent() const;

  const std::vector<std::string>&	getMenuEntries() const;
  unsigned int				getSelectedEntry() const;
};

#endif /* !SHOME_H_ */

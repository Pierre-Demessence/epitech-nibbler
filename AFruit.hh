#ifndef AFRUIT_H_
# define AFRUIT_H_

# include	"AEntity.hh"
# include	"ASnake.hh"

class AFruit : public AEntity
{
public:
  enum	EFruitType
    {
      APPLE,
      KIWI,
      BANANA,
      TOMATO,
      UNKNOWN
    };

protected:
  AFruit(const AFruit&);
  AFruit&	operator=(const AFruit&);

  EFruitType typeFruit;

public:
  AFruit();
  ~AFruit();
  void update();
  EFruitType getTypeFruit() const;
  virtual void	eaten(ASnake &snake) const = 0;
  virtual AFruit *clone() const = 0;
};

#endif /* !AFRUIT_H_ */

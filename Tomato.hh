#ifndef TOMATO_H_
# define TOMATO_H_

# include	"AFruit.hh"

class Tomato : public AFruit
{
protected:
  Tomato(const Tomato&);
  Tomato&	operator=(const Tomato&);

public:
  Tomato();
  ~Tomato();

  void eaten(ASnake &snake) const;
  AFruit *clone() const;
};

#endif /* !TOMATO_H_ */

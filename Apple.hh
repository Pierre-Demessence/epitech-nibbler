#ifndef APPLE_H_
# define APPLE_H_

# include	"AFruit.hh"

class Apple : public AFruit
{
protected:
  Apple(const Apple&);
  Apple&	operator=(const Apple&);

public:
  Apple();
  ~Apple();

  void eaten(ASnake &snake) const;
  AFruit *clone() const;
};

#endif /* !APPLE_H_ */

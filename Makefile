CC	=	g++

RM	=	rm -f

CXXFLAGS	+=	-Wextra -Wall
CXXFLAGS	+=	-Werror
CXXFLAGS 	+=	-ansi -pedantic
#CXXFLAGS	+=	-ggdb3 -O0
CXXFLAGS	+=	$(INCLUDE)

INCLUDE =

LIBDIR	=
LIB	=	-ldl

LDFLAGS	+=	$(LIBDIR) $(LIB) -rdynamic

NAME	=	nibbler

SRCS	=	main.cpp		\
		GameEngine.cpp		\
\
		Map.cpp			\
		AEntity.cpp		\
		Wall.cpp		\
\
		ASnake.cpp		\
		SnakePlayer.cpp		\
		SnakeAI.cpp		\
\
		FruitFactory.cpp	\
		AFruit.cpp		\
		Apple.cpp		\
		Kiwi.cpp		\
		Banana.cpp		\
		Tomato.cpp		\
\
		StateManager.cpp	\
		IState.cpp		\
		SHome.cpp		\
		SCredits.cpp		\
		SGame.cpp		\
		SMapOptions.cpp		\
		SPause.cpp		\
		SGameOver.cpp		\
		SScore.cpp		\
\
		IRenderer.cpp		\
		RendererException.cpp

OBJS	=	$(SRCS:.cpp=.o)

all:		libSDL libOpenGL libNCurses $(NAME)

$(NAME):	$(OBJS)
		$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

libSDL:
		make -C ./libSDL/
		ln -fs ./libSDL/lib_nibbler_SDL.so

libOpenGL:
		make -C ./libOpenGL/
		ln -fs ./libOpenGL/lib_nibbler_OpenGL.so

libNCurses:
		make -C ./libNCurses/
		ln -fs ./libNCurses/lib_nibbler_NCurses.so

clean:
		$(RM) $(OBJS)
		make clean -C ./libSDL/
		make clean -C ./libOpenGL/
		make clean -C ./libNCurses/

fclean:		clean
		$(RM) $(NAME)
		$(RM) lib_nibbler_SDL.so
		$(RM) lib_nibbler_OpenGL.so
		$(RM) lib_nibbler_NCurses.so
		make fclean -C ./libSDL/
		make fclean -C ./libOpenGL/
		make fclean -C ./libNCurses/

re:		fclean all

run:		$(NAME)
		./$(NAME) $(PARAMS)

debug:		$(NAME)
		valgrind --track-origins=yes $(OPTIONS) ./$(NAME) $(PARAMS)

.PHONY:		all libSDL libOpenGL libNCurses clean fclean re run debug

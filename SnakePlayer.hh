#ifndef SNAKEPLAYER_H_
# define SNAKEPLAYER_H_

# include	"ASnake.hh"

class	SnakePlayer : public ASnake
{
private:
  SnakePlayer(const SnakePlayer&);
  SnakePlayer&	operator=(const SnakePlayer&);

public:
  SnakePlayer(std::pair<int, int> coords);
  ~SnakePlayer();

  void	updateDirection(const Map&, const std::vector<ASnake *>&);
};

#endif /* !SNAKEPLAYER_H_ */

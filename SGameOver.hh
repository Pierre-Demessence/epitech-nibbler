#ifndef SGAMEOVER_H_
# define SGAMEOVER_H_

# include	"IState.hh"
# include	"ASnake.hh"

class		SGameOver : public IState
{
private:
  SGameOver(const SGameOver&);
  SGameOver&	operator=(const SGameOver&);
  std::vector<ASnake*>	deadSnakes;

public:
  SGameOver(const std::vector<ASnake*>&);
  ~SGameOver();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  bool	isTransparent() const;
};

#endif /* !SGAMEOVER_H_ */

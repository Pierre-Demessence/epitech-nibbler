#include "SScore.hh"

//----- ----- Constructors ----- ----- //
SScore::SScore(const std::vector<ASnake*>& snakes)
{
  this->deadSnakes = snakes;
}

//----- ----- Destructor ----- ----- //
SScore::~SScore()
{
  ASnake *snake;

  while (this->deadSnakes.size())
    {
      snake = this->deadSnakes.back();
      this->deadSnakes.pop_back();
      delete snake;
    }
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SScore::isTransparent() const
{
  return (false);
}

const std::vector<ASnake *> &SScore::getDeadSnakes() const
{
  return (this->deadSnakes);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	SScore::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;

  key = renderer.getEvent();

  if (key == IRenderer::ESCAPE ||
      key == IRenderer::ENTER ||
      key == IRenderer::SPACE)
    {
      sm.popState(&renderer);
      sm.popState(&renderer);
    }
}

void	SScore::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

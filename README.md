# ===== ===== ===== #
# Pour les libs :
- Compiler au minimum le fichier principal "[LIB_NAME]Renderer.cpp" (Exemple : "SDLRenderer.cpp").
- D'autres classes peuvent être dedans si besoin.
- Le fichier principal DOIT contenir une FONCTION "getInstance()". Elle doit être écrit comme ceci :

```
#!c++

extern "C"
IRenderer*	getInstance()
{
	return (new SDLRenderer()); // A changer selon la lib bien entendu.
}
```
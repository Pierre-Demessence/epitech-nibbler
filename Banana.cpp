#include "Banana.hh"

//----- ----- Constructors ----- ----- //
Banana::Banana() :
  AFruit()
{
  this->type = AEntity::FRUIT;
  this->typeFruit = AFruit::BANANA;;
}

//----- ----- Destructor ----- ----- //
Banana::~Banana()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void   Banana::eaten(ASnake &snake) const
{
  snake.setSpeed(snake.getSpeed() - 1);
}

AFruit *Banana::clone() const
{
  return (new Banana());
}

#ifndef GAMEENGINE_H_
# define GAMEENGINE_H_

# include	<vector>
# include	<string>

# include	"IRenderer.hh"
# include	"StateManager.hh"

// Error Handling :
# define	E_NORENDER	0x01
# define	E_BADINIT	0x02
// end

# define	FPS		60

class		GameEngine
{
private:
  std::vector<IRenderer*>	renderers;
  int				rendererIndex;
  StateManager			stateManager;
  bool				quit;

  GameEngine(const GameEngine&);
  GameEngine&	operator=(const GameEngine&);

  int		update();
  void		render();

public:
  GameEngine();
  ~GameEngine();

  int	loadLibrary(const std::string& path);
  int	run();
  void	setQuit(bool);
  void	nextRenderer();
  void	prevRenderer();
};

#endif /* !GAMEENGINE_H_ */

#ifndef ISTATE_H_
# define ISTATE_H_

# include	"StateManager.hh"

class		StateManager;

class		IState
{
private:
  IState(const IState&);
  IState&	operator=(const IState&);

public:
  IState();
  virtual	~IState();

  virtual void	update(IRenderer&, StateManager&) = 0;
  virtual void	render(IRenderer&) const = 0;
  virtual bool	isTransparent() const = 0;
};

#endif /* !ISTATE_H_ */

#include	"SHome.hh"
#include	"SCredits.hh"
#include	"SMapOptions.hh"

//----- ----- Constructors ----- ----- //
SHome::SHome()
{
  this->menuEntries.push_back("New Game");
  this->menuEntries.push_back("Credits");
  this->menuEntries.push_back("Quit");
  this->menuActions.push_back(&SHome::menuPlay);
  this->menuActions.push_back(&SHome::menuCredits);
  this->menuActions.push_back(&SHome::menuQuit);
  this->selectedEntry = 0;
}

//----- ----- Destructor ----- ----- //
SHome::~SHome()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SHome::isTransparent() const
{
  return (false);
}

const std::vector<std::string>&	SHome::getMenuEntries() const
{
  return (this->menuEntries);
}

unsigned int			SHome::getSelectedEntry() const
{
  return (this->selectedEntry);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //

void	SHome::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;

  key = renderer.getEvent();

  if (key == IRenderer::UP)
    this->selectedEntry--;
  else if (key == IRenderer::DOWN)
    this->selectedEntry++;

  if (this->selectedEntry < 0)
    this->selectedEntry = this->menuEntries.size() - 1;
  if (this->selectedEntry == static_cast<int>(this->menuEntries.size()))
    this->selectedEntry = 0;

  if (key == IRenderer::ENTER)
    (this->*this->menuActions[this->selectedEntry])(renderer, sm);

  if (key == IRenderer::PAGE_UP)
    sm.prevRenderer();
  else if (key == IRenderer::PAGE_DOWN)
    sm.nextRenderer();

  if (key == IRenderer::ESCAPE)
    sm.popState();
}

void	SHome::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

void	SHome::menuPlay(IRenderer& renderer, StateManager& sm)
{
  sm.pushState(new SMapOptions(), &renderer);
}

void	SHome::menuOptions(IRenderer&, StateManager&)
{
}

void	SHome::menuCredits(IRenderer& renderer, StateManager& sm)
{
  sm.pushState(new SCredits(), &renderer);
}

void	SHome::menuQuit(IRenderer&, StateManager& sm)
{
  sm.popState();
}

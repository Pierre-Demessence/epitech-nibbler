#ifndef WALL_H_
# define WALL_H_

# include	"AEntity.hh"

class	Wall : public AEntity
{
protected:
  Wall(const Wall&);
  Wall&	operator=(const Wall&);

public:
  Wall(std::pair<int, int>);
  ~Wall();

  void	update();
};

#endif /* !WALL_H_ */

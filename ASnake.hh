#ifndef ASNAKE_H_
# define ASNAKE_H_

# include	<vector>
# include	"AEntity.hh"
# include	"Map.hh"

class Map;

class	ASnake : public AEntity
{
private:
  ASnake(const ASnake&);
  ASnake&	operator=(const ASnake&);

protected:
  char	direction;
  char	directionNext;
  float	speed;
  int	grownb;
  unsigned int mad;
  unsigned int fruitsEat;
  unsigned int snakeKill;

public:
  ~ASnake();
  ASnake(std::pair<int, int> coords);

  static const int MIN_SPEED;
  static const int MAX_SPEED;

  int	getDirection() const;
  int	getSpeed() const;
  unsigned int getScore() const;
  unsigned int getMad() const;

  void	addFruit();
  void	addKill();
  void	setDirection(int);
  void	setSpeed(int);
  void	addMad(unsigned int);

  void	grow(int);
  void	turnLeft();
  void	turnRight();
  void	update(const Map&, const std::vector<ASnake *>&);
  virtual void	updateDirection(const Map&, const std::vector<ASnake *>&) = 0;
  bool	isByteHimSelf() const;
  bool	isCollide(const AEntity&) const;
};

#endif /* !ASNAKE_H_ */

#include	<vector>
#include	"SnakeAI.hh"

char	SnakeAI::difficultyAI = 1;

//----- ----- Constructors ----- ----- //
SnakeAI::SnakeAI(std::pair<unsigned int, unsigned int> coords)
  : ASnake(coords)
{
  this->type = AEntity::SNAKEAI;
}

//----- ----- Destructor ----- ----- //
SnakeAI::~SnakeAI()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //

void	SnakeAI::getNextFruit(const Map &map)
{
  unsigned int	min;
  unsigned int	res;
  int	tempo;

  min = map.getWidth() + map.getHeight();
  for (unsigned int i = 0; i < map.getEntities().size(); ++i)
    {
      if (map.getEntities()[i]->getEntityType() != AEntity::FRUIT)
	continue ;
      res = 0;
      tempo = map.getEntities()[i]->getCoordinates()[0].first - this->coordinates[0].first;
      tempo = (tempo < 0 ? tempo * -1 : tempo);
      res += tempo;
      tempo = map.getEntities()[i]->getCoordinates()[0].second - this->coordinates[0].second;
      tempo = (tempo < 0 ? tempo * -1 : tempo);
      res += tempo;
      if (res < min)
	{
	  min = res;
	  this->nextFruit = map.getEntities()[i]->getCoordinates()[0];
	}
    }
}

void	SnakeAI::getNextPos(std::pair<unsigned int, unsigned int> &nextPosTest)
{
  nextPosTest.first = this->coordinates[0].first;
  nextPosTest.second = this->coordinates[0].second;
  switch (this->direction)
    {
    case 0:
      ++(nextPosTest.first);
      break;
    case 1:
      --(nextPosTest.second);
      break;
    case 2:
      --(nextPosTest.first);
      break;
    case 3:
      ++(nextPosTest.second);
      break;
    }
}

void	SnakeAI::getNextRight()
{
  --direction;
  direction = (direction < 0 ? 3 : direction);
  getNextPos(this->nextRight);
  ++direction;
  direction = (direction > 3 ? 0 : direction);
}

void	SnakeAI::getNextLeft()
{
  ++direction;
  direction = (direction > 3 ? 0 : direction);
  getNextPos(this->nextLeft);
  --direction;
  direction = (direction < 0 ? 3 : direction);
}

char	SnakeAI::easyLevelAI()
{
  char	flag;

  flag = 0;
  if (something_ahead)
    {
      if (!something_right)
	flag = 1;
      else if (!something_left)
	flag = -1;
    }
  return (flag);
}

char	SnakeAI::mediumLevelAI(const Map& map)
{
  char	flag;

  flag = 0;
  if (something_ahead)
    {
      if (!something_right && something_left)
	flag = 1;
      else if (!something_left && something_right)
	flag = -1;
      else
	switch (this->direction)
	  {
	  case 0:
	    flag = (this->coordinates[0].second < (map.getHeight() >> 1) ? 1 : -1);
	    break ;
	  case 1:
	    flag = (this->coordinates[0].first > (map.getWidth() >> 1) ? -1 : 1);
	    break ;
	  case 2:
	    flag = (this->coordinates[0].second > (map.getHeight() >> 1) ? 1 : -1);
	    break ;
	  case 3:
	    flag = (this->coordinates[0].first < (map.getWidth() >> 1) ? -1 : 1);
	    break ;
	  }
    }
  return (flag);
}

void	SnakeAI::calcHardMode(const Map &map,
			      const std::pair<unsigned int, unsigned int> &root,
			      std::vector<std::pair<unsigned int, unsigned int> >& checkCases,
			      unsigned int &result)
{
  std::pair<unsigned int, unsigned int> pair;
  static unsigned int maxTest = map.getWidth() * (map.getHeight() >> 2);

  if (root.first > map.getWidth())
    return ;
  if (root.second >= map.getHeight())
    return ;
  for (unsigned int i = 0; i < checkCases.size(); ++i)
    {
      if (root == checkCases[i])
	return ;
    }
  if (map.isCaseFree(root))
    {
      if (result >= maxTest)
	return ;
      ++result;
      checkCases.push_back(std::pair<unsigned int, unsigned int>(root.first, root.second));
      pair.first = root.first + 1;
      pair.second = root.second;
      if (map.isCaseGood(pair) && map.isCaseSnakesFree(pair))
	this->calcHardMode(map, pair, checkCases, result);
      pair.first = root.first - 1;
      pair.second = root.second;
      if (map.isCaseGood(pair) && map.isCaseSnakesFree(pair))
	this->calcHardMode(map, pair, checkCases, result);
      pair.first = root.first;
      pair.second = root.second + 1;
      if (map.isCaseGood(pair) && map.isCaseSnakesFree(pair))
	this->calcHardMode(map, pair, checkCases, result);
      pair.first = root.first;
      pair.second = root.second - 1;
      if (map.isCaseGood(pair) && map.isCaseSnakesFree(pair))
	this->calcHardMode(map, pair, checkCases, result);
    }
}

char	SnakeAI::hardLevelAI(const Map& map)
{
  char	flag = 0;
  std::vector<std::pair<unsigned int, unsigned int> > vectorCheckCases;
  unsigned int resRight = 0;
  unsigned int resLeft = 0;

  if (something_ahead)
    {
      if (!something_right && something_left)
	flag = 1;
      else if (!something_left && something_right)
	flag = -1;
      else
	{
	  calcHardMode(map, this->nextRight, vectorCheckCases, resRight);
	  vectorCheckCases.clear();
	  calcHardMode(map, this->nextLeft, vectorCheckCases, resLeft);
	  vectorCheckCases.clear();
	  if (resLeft > resRight)
	    flag = -1;
	  else if (resLeft < resRight)
	    flag = 1;
	  else
	    switch (this->direction)
	      {
	      case 0:
		flag = (this->coordinates[0].second < (map.getHeight() >> 1) ? 1 : -1);
		break ;
	      case 1:
		flag = (this->coordinates[0].first > (map.getWidth() >> 1) ? -1 : 1);
		break ;
	      case 2:
		flag = (this->coordinates[0].second > (map.getHeight() >> 1) ? 1 : -1);
		break ;
	      case 3:
		flag = (this->coordinates[0].first < (map.getWidth() >> 1) ? -1 : 1);
		break ;
	      }
	}
    }
  return (flag);
}

void	SnakeAI::move(const Map &map)
{
  char	flag = 0;

  if (SnakeAI::difficultyAI == 1)
    flag = easyLevelAI();
  else if (SnakeAI::difficultyAI == 2)
    flag = mediumLevelAI(map);
  else if (SnakeAI::difficultyAI == 3)
    flag = hardLevelAI(map);
  if (flag == 0)
    {
      if (weight > 0 && !something_left)
	flag = -1;
      else if (weight < 0 && !something_right)
	flag = 1;
    }
  if (this->difficultyAI >= 2 && this->mad && flag)
    flag *= -1;
  if (flag == 1)
    this->turnRight();
  else if (flag == -1)
    this->turnLeft();
}

void	SnakeAI::blockMap(const Map &map, const std::vector<ASnake *>& snakes)
{
  std::pair<unsigned int, unsigned int> others;

  for (unsigned int i = 0; i < map.getEntities().size(); ++i)
    {
      if (map.getEntities()[i]->getEntityType() != AEntity::FRUIT)
	{
	  if (map.getEntities()[i]->getCoordinates()[0] == nextPos)
	    this->something_ahead = true;
	  if (map.getEntities()[i]->getCoordinates()[0] == nextRight)
	    this->something_right = true;
	  if (map.getEntities()[i]->getCoordinates()[0] == nextLeft)
	    this->something_left = true;
	}
    }
  for (unsigned int i = 0; i < snakes.size(); ++i)
    {
      for (unsigned int j = 0; j < snakes[i]->getCoordinates().size(); ++j)
  	{
  	  others = snakes[i]->getCoordinates()[j];
  	  if (this->nextPos == others)
	    this->something_ahead = true;
	  if (this->nextRight == others)
	    this->something_right = true;
	  if (this->nextLeft == others)
	    this->something_left = true;
  	}
    }
}

void	SnakeAI::updateDirection(const Map& map, const std::vector<ASnake *>& snakes)
{
  this->weight = 0;
  this->something_ahead = false;
  this->something_left = false;
  this->something_right = false;

  getNextPos(this->nextPos);
  getNextLeft();
  getNextRight();
  getNextFruit(map);
  this->blockMap(map, snakes);
  switch (this->direction)
    {
    case 0:
      if (this->coordinates[0].first == this->nextFruit.first)
	weight += (this->coordinates[0].second > this->nextFruit.second ? 1 : -1);
      break ;
    case 1:
      if (this->coordinates[0].second == this->nextFruit.second)
	weight += (this->coordinates[0].first > this->nextFruit.first ? 1 : -1);
      break ;
    case 2:
      if (this->coordinates[0].first == this->nextFruit.first)
	weight += (this->coordinates[0].second < this->nextFruit.second ? 1 : -1);
      break ;
    case 3:
      if (this->coordinates[0].second == this->nextFruit.second)
	weight += (this->coordinates[0].first < this->nextFruit.first ? 1 : -1);
      break ;
    }
  this->move(map);
}

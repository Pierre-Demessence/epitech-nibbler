#include "AFruit.hh"

//----- ----- Constructors ----- ----- //
AFruit::AFruit()
{
  this->type = AEntity::UNKNOWN;
  this->typeFruit = AFruit::UNKNOWN;
}

//----- ----- Destructor ----- ----- //
AFruit::~AFruit()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	AFruit::update()
{
}

AFruit::EFruitType AFruit::getTypeFruit() const
{
  return (this->typeFruit);
}

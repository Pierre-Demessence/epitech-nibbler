
#include <vector>
#include <string>
#include <csignal>
#include <stropts.h>
#include <termios.h>
#include <sys/ioctl.h>
#include "NCursesRenderer.hh"

bool		NCursesRenderer::resize = true;

//----- ----- Constructors ----- ----- //
NCursesRenderer::NCursesRenderer()
{
  this->ready = false;
  this->keys.push_back(std::pair<int, IRenderer::EKey>(27, IRenderer::ESCAPE));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_UP, IRenderer::UP));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_DOWN, IRenderer::DOWN));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_RIGHT, IRenderer::RIGHT));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_LEFT, IRenderer::LEFT));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(10, IRenderer::ENTER));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(32, IRenderer::SPACE));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(-1, IRenderer::NONE));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_PPAGE, IRenderer::PAGE_UP));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_NPAGE, IRenderer::PAGE_DOWN));
  this->keys.push_back(std::pair<int, IRenderer::EKey>(KEY_RESIZE, IRenderer::NONE));
  ESCDELAY = 20;
}

//----- ----- Destructor ----- ----- //
NCursesRenderer::~NCursesRenderer()
{
  if (this && this->ready)
    {
      this->pause();
    }
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	NCursesRenderer::isReady() const
{
  return (this->ready);
}

//----- ----- Setters ----- ----- //
void	NCursesRenderer::my_resize(int sign)
{
  if (signal(SIGWINCH, SIG_IGN) != SIG_ERR)
    {
      clear();
      NCursesRenderer::resize = true;
      (void)signal(SIGWINCH, NCursesRenderer::my_resize);
    }
  (void)sign;
}

NCursesRenderer::EKey	NCursesRenderer::getEvent() const
{
  int			c;
  unsigned int		size;

  c = wgetch(this->win);
  size = this->keys.size();
  for (unsigned int i = 0; i < size; ++i)
    {
      if (c == this->keys[i].first)
	return (this->keys[i].second);
    }
  return (IRenderer::OTHER);
}

void	NCursesRenderer::init()
{
  if ((signal(SIGWINCH, NCursesRenderer::my_resize)) == SIG_ERR)
    return ;
  if ((this->win = initscr()))
    {
      curs_set(0);
      cbreak();
      keypad(this->win, TRUE);
      noecho();
      start_color();
      nodelay(this->win, 1);
      this->ready = true;
    }
}

void	NCursesRenderer::pause()
{
  clear();
  curs_set(1);
  endwin();
  signal(SIGWINCH, SIG_DFL);
  this->ready = false;
}

void	NCursesRenderer::drawBorder()
{
  unsigned int	y;
  unsigned int	x;
  struct winsize term;

  if (NCursesRenderer::resize)
    {
      if (ioctl(0, TIOCGWINSZ, &term) != -1)
	{
	  this->maxy = term.ws_row;
	  this->maxx = term.ws_col;
	  resizeterm(this->maxy, this->maxx);
	}
      NCursesRenderer::resize = false;
    }
  move(0, 0);
  addch(ACS_ULCORNER);
  for (y = 2; y < this->maxx; ++y)
    addch(ACS_HLINE);
  addch(ACS_URCORNER);
  move(this->maxy - 1, 0);
  addch(ACS_LLCORNER);
  for (y = 2; y < this->maxx; ++y)
    addch(ACS_HLINE);
  addch(ACS_LRCORNER);
  for (x = 1; x < this->maxy - 1; ++x)
    {
      move(x, 0);
      addch(ACS_VLINE);
    }
  for (x = 1; x < this->maxy - 1; ++x)
    {
      move(x, this->maxx - 1);
      addch(ACS_VLINE);
    }
}

void	NCursesRenderer::drawSquare(unsigned int startx, unsigned int starty,
				    unsigned int sizex, unsigned int sizey)
{
  unsigned int y;

  move(starty, startx);
  init_pair(2, COLOR_RED, COLOR_BLACK);
  attron(COLOR_PAIR(2));
  for (y = 0; y < sizex; ++y)
    addch('*');
  move(starty + sizey, startx);
  for (y = 0; y < sizex; ++y)
    addch('*');
  for (y = starty; y < starty + sizey; ++y)
    {
      move(y, startx);
      addch('*');
    }
  for (y = starty; y < starty + sizey; ++y)
    {
      move(y, startx + sizex - 1);
      addch('*');
    }
  attroff(COLOR_PAIR(2));
}

void	NCursesRenderer::renderState(const SHome&state)
{
  std::vector<std::string> menuEntries = state.getMenuEntries();
  unsigned int	posx;
  unsigned int	posy;
  unsigned int	memoy;
  unsigned int	maxlen;

  init_pair(1, COLOR_RED, COLOR_BLUE);
  attron(COLOR_PAIR(1));
  this->drawBorder();
  attroff(COLOR_PAIR(1));
  posy = (this->maxy >> 1) - (menuEntries.size() >> 1) * 2 - 1;
  memoy = posy;
  maxlen = 0;
  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      posx = (this->maxx >> 1) - (menuEntries[i].size() >> 1);
      if (menuEntries[i].size() > maxlen)
	maxlen = menuEntries[i].size();
      posy += 2;
      move(posy, posx);
      if (i == state.getSelectedEntry())
	{
	  attron(A_UNDERLINE);
	  attron(A_BLINK);
	}
      printw("%s", menuEntries[i].c_str());
      if (i == state.getSelectedEntry())
	{
	  attroff(A_UNDERLINE);
	  attroff(A_BLINK);
	}
    }
  drawSquare((this->maxx >> 1) - (maxlen >> 1) - 2, memoy,
	     maxlen + 4, (menuEntries.size() * 2) + 2);
}

void	NCursesRenderer::renderState(const SCredits& state)
{
  int	posy;
  int	posx;
  std::string test("nibbler");
  std::vector<std::string> vect;

  vect.push_back("rouchy_a");
  vect.push_back("demess_p");
  vect.push_back(" taieb_t");

  (void)state;
  init_pair(1, COLOR_CYAN, COLOR_BLACK);
  attron(COLOR_PAIR(1));
  this->drawBorder();
  posy = (this->maxy >> 1) - 3;
  posx = (this->maxx >> 1) - 1 - (test.size() >> 1);
  drawSquare(posx, posy, test.size() + 2, 2);
  attron(COLOR_PAIR(1));
  move(posy + 1, posx + 1);
  printw("%s", test.c_str());
  posy += 4;
  move(posy, posx + 3);
  printw("BY");
  ++posy;
  posx = (this->maxx >> 1) - 4;
  for (unsigned int i = 0; i < vect.size(); ++i)
    {
      ++posy;
      move(posy, posx);
      printw("%s", vect[i].c_str());
    }
  attroff(COLOR_PAIR(1));
}

std::string NCursesRenderer::getRendererName() const
{
  return (std::string("NCurses"));
}

void	NCursesRenderer::renderState(const SGameOver&state)
{
  int	posx;
  int	posy;

  (void)state;
  init_pair(1, COLOR_BLACK, COLOR_RED);
  posy = (this->maxy >> 1) - 1;
  posx = (this->maxx >> 1) - 7;
  drawSquare(posx, posy, 17, 2);
  move(posy + 1, posx + 2);
  attron(COLOR_PAIR(1));
  printw("  Game over  ");
  attroff(COLOR_PAIR(1));
}

void	NCursesRenderer::renderState(const SPause&state)
{
  int	posx;
  int	posy;

  (void)state;
  posy = (this->maxy >> 1) - 1;
  posx = (this->maxx >> 1) - 4;
  drawSquare(posx, posy, 9, 2);
  move(posy + 1, posx + 2);
  printw("Pause");
}

void	NCursesRenderer::drawSnakes(const SGame &state, unsigned int beginx, unsigned int beginy)
{
  unsigned int max;
  std::vector<ASnake *> snakes;
  std::vector<std::pair<unsigned int, unsigned int> > positions;
  unsigned int posy;
  unsigned int posx;

  snakes = state.getSnakes();
  max = snakes.size();
  init_pair(4, COLOR_GREEN, COLOR_BLACK);
  init_pair(2, COLOR_RED, COLOR_BLACK);
  for (unsigned int i = 0; i < max; ++i)
    {
      positions = snakes[i]->getCoordinates();
      if (snakes[i]->getEntityType() == AEntity::SNAKEPLAYER)
	attron(COLOR_PAIR(4));
      else if (snakes[i]->getEntityType() == AEntity::SNAKEAI)
	attron(COLOR_PAIR(2));
      posy =  beginy + positions[0].second;
      posx =  beginx + positions[0].first;
      move(posy, posx);
      addch('O');
      for(unsigned int j = 1; j < positions.size(); ++j)
	{
	  posy =  beginy + positions[j].second;
	  posx =  beginx + positions[j].first;
	  if (posy > this->maxy || posx > this->maxx)
	    continue ;
	  move(posy, posx);
	  addch('X');
	}
      if (snakes[i]->getEntityType() == AEntity::SNAKEPLAYER)
	attroff(COLOR_PAIR(4));
      else if (snakes[i]->getEntityType() == AEntity::SNAKEAI)
	attroff(COLOR_PAIR(2));
    }
}

void NCursesRenderer::drawFruits(AFruit *fruit) const
{
  if (fruit->getTypeFruit() == AFruit::APPLE)
    addch('a');
  else if (fruit->getTypeFruit() == AFruit::KIWI)
    addch('k');
  else if (fruit->getTypeFruit() == AFruit::BANANA)
    addch('b');
  else if (fruit->getTypeFruit() == AFruit::TOMATO)
    addch('m');
}

void NCursesRenderer::drawEntities(const SGame &state, unsigned int beginx, unsigned int beginy)
{
  std::vector<std::pair<unsigned int, unsigned int> > positions;
  std::vector<AEntity *> entities;
  unsigned int max;

  entities = state.getMapEntities();
  max = entities.size();
  for (unsigned int i = 0; i < max; ++i)
    {
      positions = entities[i]->getCoordinates();
      for(unsigned int j = 0; j < positions.size(); ++j)
	{
	  move(beginy + positions[j].second, beginx + positions[j].first);
	  if (entities[i]->getEntityType() == AEntity::WALL)
	    addch('#');
	  else if (entities[i]->getEntityType() == AEntity::FRUIT)
	    this->drawFruits(dynamic_cast<AFruit *>(entities[i]));
	}
    }
}

void NCursesRenderer::cleanmap() const
{
  for (unsigned int i = 0; i < this->maxx; ++i)
    {
      for (unsigned int j = 0; j < this->maxy; ++j)
	{
	  move(j, i);
	  addch(' ');
	}
    }
}

void	NCursesRenderer::renderState(const SGame&state)
{
  static bool render = false;
  unsigned int	sizex;
  unsigned int	sizey;
  unsigned int posx;
  unsigned int posy;

  if (render == true)
    return ;
  render = true;
  sizex = state.getMapWidth();
  sizey = state.getMapHeight();
  move(10, 10);
  init_pair(1, COLOR_RED, COLOR_BLUE);
  attron(COLOR_PAIR(1));
  this->drawBorder();
  attroff(COLOR_PAIR(1));
  posx = (this->maxx >> 1) - (sizex >> 1);
  posy = (this->maxy >> 1) - (sizey >> 1);
  cleanmap();
  drawEntities(state, posx, posy);
  drawSnakes(state, posx, posy);
  render = false;
}

void	NCursesRenderer::renderState(const SScore&state)
{
  int	posx;
  int	posy;

  (void)state;
  init_pair(1, COLOR_BLACK, COLOR_RED);
  attron(COLOR_PAIR(1));
  this->drawBorder();
  attroff(COLOR_PAIR(1));
  posy = (this->maxy >> 1) - 1;
  posx = (this->maxx >> 1) - 7;
  drawSquare(posx, posy, 17, 2);
  move(posy + 1, posx + 4);
  attron(COLOR_PAIR(1));
  printw("  SCORE  ");
  attroff(COLOR_PAIR(1));
  for (unsigned int i = 0; i < state.getDeadSnakes().size(); ++i)
    {
      move(posy + 5 + i, posx - 2);
      if (state.getDeadSnakes()[i]->getEntityType() == AEntity::SNAKEAI)
	{
	  attron(COLOR_PAIR(1));
	  printw("SNAKE AI [%d] => [%d]", i, state.getDeadSnakes()[i]->getScore());
	  attroff(COLOR_PAIR(1));
	}
      else
	printw("SNAKE PLAYER [%d] => [%d]", i, state.getDeadSnakes()[i]->getScore());
    }
}

void	NCursesRenderer::renderState(const SMapOptions& state)
{
  std::vector<std::string> menuEntries = state.getMenuEntries();
  unsigned int	posx;
  unsigned int	posy;
  unsigned int	maxlen;

  init_pair(1, COLOR_RED, COLOR_BLUE);
  attron(COLOR_PAIR(1));
  this->drawBorder();
  attroff(COLOR_PAIR(1));
  posy = (this->maxy >> 1) - (menuEntries.size() >> 1) * 2 - 1;
  posx = (this->maxx >> 1) - 5;
  maxlen = 0;
  for (unsigned int i = posy; i < posy + (2 * menuEntries.size()); ++i)
    {
      for (unsigned int j = posx - 30; j < posx + 30; ++j)
	{
	  move(i, j);
	  addch(' ');
	}
    }
  move(posy, posx);
  printw("Map options :");
  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      posx = (this->maxx >> 1) - (menuEntries[i].size() >> 1);
      if (menuEntries[i].size() > maxlen)
  	maxlen = menuEntries[i].size();
      posy += 2;
      move(posy, posx);
      if (i == state.getSelectedEntry())
	attron(A_UNDERLINE);
      printw("%s", menuEntries[i].c_str());
      if (i == state.getSelectedEntry())
	attroff(A_UNDERLINE);
    }
}



void	NCursesRenderer::renderBegin()
{
}

void	NCursesRenderer::renderEnd()
{
}

void	NCursesRenderer::renderTransition()
{
  clear();
}

extern "C"
IRenderer*		getInstance()
{
  return (new NCursesRenderer());
}

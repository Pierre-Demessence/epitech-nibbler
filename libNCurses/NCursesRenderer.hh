#ifndef NCURSESRENDERER_H_
# define NCURSESRENDERER_H_

# include	<ncurses.h>
# include	<term.h>
# include	<string>
# include	"IRenderer.hh"
# include	"SHome.hh"
# include	"SCredits.hh"
# include	"SMapOptions.hh"
# include	"SGame.hh"
# include	"SScore.hh"
# include	"AEntity.hh"
# include	"ASnake.hh"
# include	"AFruit.hh"

class		NCursesRenderer : public IRenderer
{
private:
  bool ready;
  WINDOW *win;
  unsigned int maxx;
  unsigned int maxy;
  std::vector<std::pair<int, IRenderer::EKey> > keys;

  NCursesRenderer(const NCursesRenderer&);
  NCursesRenderer& operator=(const NCursesRenderer&);
  void drawBorder();
  void drawFruits(AFruit *) const;
  void cleanmap() const;
  void drawSnakes(const SGame &state, unsigned int beginx, unsigned int beginy);
  void drawEntities(const SGame &state, unsigned int beginx, unsigned int beginy);
  void drawSquare(unsigned int startx, unsigned int starty,
		  unsigned int sizex, unsigned int sizey);

public:
  NCursesRenderer();
  ~NCursesRenderer();

  EKey	getEvent() const;
  std::string getRendererName() const;

  void	renderState(const SHome&);
  void	renderState(const SCredits&);
  void	renderState(const SGameOver&);
  void	renderState(const SPause&);
  void	renderState(const SGame&);
  void	renderState(const SScore&);
  // void	renderState(const SOptions&);
  void	renderState(const SMapOptions&);

  void	renderBegin();
  void	renderEnd();
  void	renderTransition();

  void	init();
  void	pause();

  bool isReady() const;


  static void my_resize(int sign);
  static bool resize;
};

#endif /* !NCURSESRENDERER_H_ */

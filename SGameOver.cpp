#include <algorithm>
#include "SGameOver.hh"
#include "SScore.hh"

//----- ----- Constructors ----- ----- //
SGameOver::SGameOver(const std::vector<ASnake*>& snakes)
{
  this->deadSnakes = snakes;
}

//----- ----- Destructor ----- ----- //
SGameOver::~SGameOver()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SGameOver::isTransparent() const
{
  return (true);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //

bool	cmp(const ASnake* lhs, const ASnake* rhs)
{
  return (lhs->getScore() < rhs->getScore());
}

void	SGameOver::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;
  std::vector<ASnake *>	snakes;

  key = renderer.getEvent();

  if (key == IRenderer::ESCAPE ||
      key == IRenderer::ENTER ||
      key == IRenderer::SPACE)
    {
      snakes = this->deadSnakes;
      sm.popState(&renderer);
      sm.popState(&renderer);
      std::sort(snakes.begin(), snakes.end(), cmp);
      std::reverse(snakes.begin(), snakes.end());
      sm.pushState(new SScore(snakes));
    }
}

void	SGameOver::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

#ifndef MAP_H_
# define MAP_H_

# include	<vector>

# include	"AEntity.hh"
# include	"ASnake.hh"

class ASnake;

# define	MIN_WIDTH	30
# define	MAX_WIDTH	65
# define	MIN_HEIGHT	30
# define	MAX_HEIGHT	65

class	Map
{
private:
  unsigned int	width;
  unsigned int	height;

  std::vector<AEntity*>	entities;
  std::vector<ASnake*>	snakes;

  Map(const Map&);
  Map&	operator=(const Map&);

public:
  static unsigned int	defaultWidth;
  static unsigned int	defaultHeight;

  Map();
  ~Map();

  unsigned int	getWidth() const;
  unsigned int	getHeight() const;

  const std::vector<AEntity*>&	getEntities() const;

  void	setWidth(unsigned int);
  void	setHeight(unsigned int);
  void	addEntity(AEntity* e);
  void	setSnakes(const std::vector<ASnake*> &);
  bool	isCaseFree(const std::pair<unsigned int, unsigned int>&) const;
  bool	isCaseGood(const std::pair<unsigned int, unsigned int>&) const;
  void	deleteEntity(unsigned int pos);

  bool	isCaseSnakesFree(const std::pair<unsigned int, unsigned int>&) const;
  void	addWallBorder();
  unsigned int	getNbFruit() const;

};

#endif /* !MAP_H_ */

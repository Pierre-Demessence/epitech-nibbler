#include "Apple.hh"

//----- ----- Constructors ----- ----- //
Apple::Apple() :
  AFruit()
{
  this->type = AEntity::FRUIT;
  this->typeFruit = AFruit::APPLE;
}

//----- ----- Destructor ----- ----- //
Apple::~Apple()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void   Apple::eaten(ASnake &snake) const
{
  snake.grow(1);
}

AFruit *Apple::clone() const
{
  return (new Apple());
}

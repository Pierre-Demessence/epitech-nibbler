#ifndef SMAPOPTIONS_H_
# define SMAPOPTIONS_H_

# include	<string>
# include	<vector>

# include	"IState.hh"

class		SMapOptions : public IState
{
private:
  std::vector<std::string>	menuEntries;
  std::vector<int>		menuData;
  std::vector<int>		menuDataMin;
  std::vector<int>		menuDataMax;
  std::vector<void (SMapOptions::*)(IRenderer&, StateManager&)>	menuActions;
  int				selectedEntry;

  SMapOptions(const SMapOptions&);
  SMapOptions&	operator=(const SMapOptions&);

  void		menuPlay(IRenderer& renderer, StateManager& sm);
  void		menuBack(IRenderer& renderer, StateManager& sm);

  static unsigned int	defaultSnakesPlayer;
  static unsigned int	defaultSnakesAI;
  static unsigned int	defaultAppleRate;
  static unsigned int	defaultKiwiRate;
  static unsigned int	defaultBananaRate;
  static unsigned int	defaultTomatoRate;

public:
  SMapOptions();
  ~SMapOptions();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  bool	isTransparent() const;

  std::vector<std::string>		getMenuEntries() const;
  unsigned int				getSelectedEntry() const;
};

#endif /* !SMAPOPTIONS_H_ */

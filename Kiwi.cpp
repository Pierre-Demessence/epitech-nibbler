#include "Kiwi.hh"

//----- ----- Constructors ----- ----- //
Kiwi::Kiwi() :
  AFruit()
{
  this->type = AEntity::FRUIT;
  this->typeFruit = AFruit::KIWI;;
}

//----- ----- Destructor ----- ----- //
Kiwi::~Kiwi()
{}

//----- ----- Operators ----- ----- //

//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void   Kiwi::eaten(ASnake &snake) const
{
  snake.setSpeed(snake.getSpeed() + 1);
}

AFruit *Kiwi::clone() const
{
  return (new Kiwi());
}

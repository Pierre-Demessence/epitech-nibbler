#include	<sstream>

#include	"SMapOptions.hh"
#include	"SnakeAI.hh"
#include	"SGame.hh"

unsigned int	SMapOptions::defaultSnakesPlayer = 1;
unsigned int	SMapOptions::defaultSnakesAI = 0;
unsigned int	SMapOptions::defaultAppleRate = 16;
unsigned int	SMapOptions::defaultKiwiRate = 4;
unsigned int	SMapOptions::defaultBananaRate = 2;
unsigned int	SMapOptions::defaultTomatoRate = 1;

//----- ----- Constructors ----- ----- //
SMapOptions::SMapOptions()
{
  this->menuEntries.push_back("Map Width");
  this->menuEntries.push_back("Map Height");
  this->menuEntries.push_back("Snake Player");
  this->menuEntries.push_back("Snake AI");
  this->menuEntries.push_back("AI Strength");
  this->menuEntries.push_back("Apple Rate");
  this->menuEntries.push_back("Kiwi Rate");
  this->menuEntries.push_back("Banana Rate");
  this->menuEntries.push_back("Tomato Rate");
  this->menuEntries.push_back("Start");
  this->menuEntries.push_back("Back");

  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(NULL);
  this->menuActions.push_back(&SMapOptions::menuPlay);
  this->menuActions.push_back(&SMapOptions::menuBack);

  this->menuData.push_back(Map::defaultWidth);
  this->menuData.push_back(Map::defaultHeight);
  this->menuData.push_back(SMapOptions::defaultSnakesPlayer);
  this->menuData.push_back(SMapOptions::defaultSnakesAI);
  this->menuData.push_back(SnakeAI::difficultyAI);
  this->menuData.push_back(SMapOptions::defaultAppleRate);
  this->menuData.push_back(SMapOptions::defaultKiwiRate);
  this->menuData.push_back(SMapOptions::defaultBananaRate);
  this->menuData.push_back(SMapOptions::defaultTomatoRate);
  this->menuData.push_back(-1);
  this->menuData.push_back(-1);

  this->menuDataMin.push_back(MIN_WIDTH);
  this->menuDataMin.push_back(MIN_HEIGHT);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(1);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(0);
  this->menuDataMin.push_back(-1);
  this->menuDataMin.push_back(-1);

  this->menuDataMax.push_back(MAX_WIDTH);
  this->menuDataMax.push_back(MAX_HEIGHT);
  this->menuDataMax.push_back(50);
  this->menuDataMax.push_back(50);
  this->menuDataMax.push_back(3);
  this->menuDataMax.push_back(20);
  this->menuDataMax.push_back(20);
  this->menuDataMax.push_back(20);
  this->menuDataMax.push_back(20);
  this->menuDataMax.push_back(-1);
  this->menuDataMax.push_back(-1);
  this->selectedEntry = this->menuEntries.size() - 2;
}

//----- ----- Destructor ----- ----- //
SMapOptions::~SMapOptions()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SMapOptions::isTransparent() const
{
  return (false);
}

std::vector<std::string>	SMapOptions::getMenuEntries() const
{
  static const std::string	strengths[3] = {"Easy", "Medium", "Hard"};
  std::vector<std::string>	res;
  std::stringstream		ss;

  for (unsigned int i = 0 ; i < this->menuEntries.size() ; i++)
    {
      ss.str("");
      ss << this->menuEntries[i];
      if (i < this->menuEntries.size() - 2)
	{
	  if (this->menuEntries[i] == "AI Strength")
	    ss << " : < " << strengths[this->menuData[i] - 1] << " >";
	  else
	    ss << " : < " << this->menuData[i] << " >";
	}
      res.push_back(ss.str());
    }

  return (res);
}

unsigned int			SMapOptions::getSelectedEntry() const
{
  return (this->selectedEntry);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //

void	SMapOptions::update(IRenderer& renderer, StateManager& sm)
{
  IRenderer::EKey	key;

  key = renderer.getEvent();

  if (key == IRenderer::UP)
    this->selectedEntry--;
  else if (key == IRenderer::DOWN)
    this->selectedEntry++;

  if (this->selectedEntry < 0)
    this->selectedEntry = this->menuEntries.size() - 1;
  if (this->selectedEntry == static_cast<int>(this->menuEntries.size()))
    this->selectedEntry = 0;

  if (key == IRenderer::LEFT)
    if (this->selectedEntry < static_cast<int>(this->menuEntries.size() - 2) &&
	this->menuData[this->selectedEntry] > this->menuDataMin[this->selectedEntry])
      --this->menuData[this->selectedEntry];

  if (key == IRenderer::RIGHT)
    if (this->selectedEntry < static_cast<int>(this->menuEntries.size() - 2) &&
	this->menuData[this->selectedEntry] < this->menuDataMax[this->selectedEntry])
      ++this->menuData[this->selectedEntry];

  if (key == IRenderer::ENTER)
    if (this->menuActions[this->selectedEntry] != NULL)
      (this->*this->menuActions[this->selectedEntry])(renderer, sm);

  if (key == IRenderer::ESCAPE)
    sm.popState(&renderer);
}

void	SMapOptions::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

void	SMapOptions::menuPlay(IRenderer& renderer, StateManager& sm)
{
  Map::defaultWidth = this->menuData[0];
  Map::defaultHeight = this->menuData[1];
  SMapOptions::defaultSnakesPlayer = this->menuData[2];
  SMapOptions::defaultSnakesAI = this->menuData[3];
  SnakeAI::difficultyAI = this->menuData[4];
  SMapOptions::defaultAppleRate = this->menuData[5];
  SMapOptions::defaultKiwiRate = this->menuData[6];
  SMapOptions::defaultBananaRate = this->menuData[7];
  SMapOptions::defaultTomatoRate = this->menuData[8];
  sm.pushState(new SGame(SMapOptions::defaultAppleRate,
			 SMapOptions::defaultKiwiRate,
			 SMapOptions::defaultBananaRate,
			 SMapOptions::defaultTomatoRate,
			 SMapOptions::defaultSnakesPlayer,
			 SMapOptions::defaultSnakesAI),
	       &renderer);
}

void	SMapOptions::menuBack(IRenderer& renderer, StateManager& sm)
{
  sm.popState(&renderer);
}

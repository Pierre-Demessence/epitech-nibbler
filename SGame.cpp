#include	<cstdlib>
#include	<ctime>

#include	"SGame.hh"
#include	"SGameOver.hh"
#include	"SnakePlayer.hh"
#include	"SnakeAI.hh"
#include	"SPause.hh"
#include	"Apple.hh"
#include	"Kiwi.hh"
#include	"Banana.hh"
#include	"Tomato.hh"

SGame::SGame(int apple, int kiwi, int banana, int tomato, int nbplayer, int nbai)
{
  unsigned int	x = 0;
  unsigned int	y = 0;
  unsigned int	xo = this->getMapWidth() / 4;
  unsigned int	yo = this->getMapHeight() / 4;

  srand(time(NULL));
  this->facto.addFruit(apple, new Apple());
  this->facto.addFruit(kiwi, new Kiwi());
  this->facto.addFruit(banana, new Banana());
  this->facto.addFruit(tomato, new Tomato());

  for (int i = 0 ; i < nbplayer + nbai ; ++i)
    {
      while (!isCaseFree(std::pair<unsigned int, unsigned int>(x, y)))
	{
	  x = (rand() % (this->getMapWidth() - xo * 2)) + xo;
	  y = (rand() % (this->getMapHeight() - yo * 2)) + yo;
	}
      if (i < nbplayer)
	this->snakes.push_back(new SnakePlayer(std::pair<unsigned int, unsigned int>(x, y)));
      else
	this->snakes.push_back(new SnakeAI(std::pair<unsigned int, unsigned int>(x, y)));
      this->snakes.back()->setSpeed(7);
    }

  this->map.setSnakes(this->snakes);
}

//----- ----- Destructor ----- ----- //
SGame::~SGame()
{
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SGame::isTransparent() const
{
  return (false);
}

unsigned int	SGame::getMapWidth() const
{
  return (this->map.getWidth());
}

unsigned int	SGame::getMapHeight() const
{
  return (this->map.getHeight());
}

const std::vector<AEntity*>&	SGame::getMapEntities() const
{
  return (this->map.getEntities());
}

const std::vector<ASnake*>&	SGame::getSnakes() const
{
  return (this->snakes);
}

unsigned int SGame::getNbCaseFree() const
{
  unsigned int res = 0;

  for (unsigned int x = 0 ; x < this->map.getWidth() ; x++)
    {
      for (unsigned int y = 0 ; y < this->map.getHeight() ; y++)
	{
	  if (this->isCaseFree(std::pair<unsigned int, unsigned int>(x, y)))
	    ++res;
	}
    }
  return (res);
}

bool	SGame::isCaseFree(const std::pair<unsigned int, unsigned int>& cmp) const
{
  for (unsigned int i = 0 ; i < this->snakes.size() ; i++)
    {
      for (unsigned int j = 0; j < this->snakes[i]->getCoordinates().size(); ++j)
	{
	  if (cmp == this->snakes[i]->getCoordinates()[j])
	    return (false);
	}
    }
  return (this->map.isCaseFree(cmp));
}


void	SGame::checkCollide()
{
  bool	flag;

  for (unsigned int i = 0 ; i < this->snakes.size() ; i++)
    {
      flag = false;
      if (this->snakes[i]->isByteHimSelf())
	flag = true;
      for (unsigned int j = 0; j < this->snakes.size(); ++j)
	if (i != j && this->snakes[i]->isCollide(*(this->snakes[j])))
	  {
	    this->snakes[j]->addKill();
	    flag = true;
	  }
      for (unsigned int j = 0; j < this->map.getEntities().size(); ++j)
	{
	  if (this->snakes[i]->isCollide(*(this->map.getEntities()[j])))
	    {
	      if (this->map.getEntities()[j]->getEntityType() != AEntity::FRUIT)
		flag = true;
	      else
		{
		  static_cast<AFruit *>(this->map.getEntities()[j])->eaten(*(this->snakes[i]));
		  this->snakes[i]->addFruit();
		  this->map.deleteEntity(j);
		}
	    }
	}
      if (flag)
	{
	  deadSnakes.push_back(this->snakes[i]);
	  this->snakes.erase(this->snakes.begin() + i);
	}
    }
}

void	SGame::endGame(StateManager &sm)
{
  for (unsigned int i = 0; i < this->snakes.size(); ++i)
    this->deadSnakes.push_back(this->snakes[i]);
  sm.pushState(new SGameOver(this->deadSnakes));
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void		SGame::update(IRenderer& renderer, StateManager& sm)
{
  static long int	tick = 0;
  IRenderer::EKey	key;
  AFruit		*fruit;
  unsigned int		r1;
  unsigned int		r2;
  unsigned int		nbFruits;
  int			tmp;

  if (this->getNbCaseFree() == 0
      || this->snakes.size() == 0)
    {
      this->endGame(sm);
      return ;
    }

  nbFruits = this->snakes.size();
  if (nbFruits > this->getNbCaseFree())
    nbFruits = this->getNbCaseFree();
  tmp = 0;
  while (this->map.getNbFruit() < nbFruits)
    {
      if (this->getNbCaseFree() == 0 || tmp == 3)
	break ;
      fruit = this->facto.pickFruit();
      if (!fruit)
	fruit = new Apple();
      r1 = rand() % this->map.getWidth();
      r2 = rand() % this->map.getHeight();
      if (isCaseFree(std::pair<unsigned int, unsigned int>(r1, r2)))
  	{
  	  fruit->addCoordinates(std::pair<unsigned int, unsigned int>(r1, r2));
  	  this->map.addEntity(fruit);
	  tmp = 0;
  	}
      ++tmp;
    }

  key = renderer.getEvent();
  switch (key)
    {
    case IRenderer::SPACE:
      sm.pushState(new SPause());
      break;
    case IRenderer::LEFT:
      for (unsigned int i = 0 ; i < this->snakes.size() ; i++)
	if (this->snakes[i]->getEntityType() == AEntity::SNAKEPLAYER)
	  this->snakes[i]->turnLeft();
      break;
    case IRenderer::RIGHT:
      for (unsigned int i = 0 ; i < this->snakes.size() ; i++)
	if (this->snakes[i]->getEntityType() == AEntity::SNAKEPLAYER)
	  this->snakes[i]->turnRight();
      break;
    case IRenderer::ESCAPE:
      this->endGame(sm);
      return ;
    default:
      break;
    }

  for (unsigned int i = 0 ; i < this->snakes.size() ; i++)
    if (tick % ((ASnake::MAX_SPEED+1) - this->snakes[i]->getSpeed()) == 0)
      this->snakes[i]->update(this->map, this->snakes);

  this->checkCollide();

  if (key == IRenderer::PAGE_UP)
    sm.prevRenderer();
  else if (key == IRenderer::PAGE_DOWN)
    sm.nextRenderer();

  ++tick;
}

void	SGame::render(IRenderer& renderer) const
{
  renderer.renderState(*this);
}

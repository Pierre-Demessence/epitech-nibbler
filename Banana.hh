#ifndef BANANA_H_
# define BANANA_H_

# include	"AFruit.hh"

class Banana : public AFruit
{
protected:
  Banana(const Banana&);
  Banana&	operator=(const Banana&);

public:
  Banana();
  ~Banana();

  void eaten(ASnake &snake) const;
  AFruit *clone() const;
};

#endif /* !BANANA_H_ */

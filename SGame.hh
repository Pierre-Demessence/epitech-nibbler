#ifndef SGAME_H_
# define SGAME_H_

# include	"ASnake.hh"
# include	"IState.hh"
# include	"Map.hh"
# include	"FruitFactory.hh"

class		SGame : public IState
{
private:
  Map			map;
  std::vector<ASnake*>	snakes;
  std::vector<ASnake*>	deadSnakes;
  FruitFactory		facto;

  SGame(const SGame&);
  SGame&	operator=(const SGame&);

  void	checkCollide();
  unsigned int getNbCaseFree() const;
  bool	isCaseFree(const std::pair<unsigned int, unsigned int>&) const;
  void	endGame(StateManager &);

public:
  // SGame();
  SGame(int apple, int kiwi, int tomato, int banana, int nbplayer, int nbai);
  ~SGame();

  bool	isTransparent() const;
  unsigned int	getMapWidth() const;
  unsigned int	getMapHeight() const;
  const std::vector<AEntity*>&	getMapEntities() const;
  const std::vector<ASnake*>&	getSnakes() const;

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
};

#endif /* !SGAME_H_ */

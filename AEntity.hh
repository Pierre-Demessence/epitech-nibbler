#ifndef AENTITY_H_
# define AENTITY_H_

# include	<vector>

class	AEntity
{
public:
  enum	EEntityType
    {
      SNAKEPLAYER,
      SNAKEAI,
      FRUIT,
      WALL,
      UNKNOWN
    };

private:
  AEntity(const AEntity&);
  AEntity&	operator=(const AEntity&);

protected:
  std::vector<std::pair<unsigned int, unsigned int> > coordinates;
  EEntityType	type;

public:
  AEntity();
  AEntity(std::pair<int, int>);
  virtual	~AEntity();

  const std::vector<std::pair<unsigned int, unsigned int> >&	getCoordinates() const;
  void		addCoordinates(std::pair<unsigned int, unsigned int>);
  EEntityType	getEntityType() const;

  bool		isCollide(const AEntity&) const;

};

#endif /* !AENTITY_H_ */

#ifndef IRENDERER_H_
# define IRENDERER_H_

# include	<string>

# define	WIN_WIDTH	1280
# define	WIN_HEIGHT	1024

class		SHome;
class		SCredits;
class		SGame;
class		SMapOptions;
class		SPause;
class		SGameOver;
class		SScore;

class		IRenderer
{
public:
  enum		EKey
    {
      NONE,
      LEFT,
      RIGHT,
      UP,
      DOWN,
      SPACE,
      ENTER,
      ESCAPE,
      PAGE_UP,
      PAGE_DOWN,
      OTHER
    };

private:
  IRenderer(const IRenderer&);
  IRenderer&	operator=(const IRenderer&);

public:
  IRenderer();
  virtual		~IRenderer();

  virtual void		renderState(const SHome&) = 0;
  virtual void		renderState(const SCredits&) = 0;
  virtual void		renderState(const SGame&) = 0;
  virtual void		renderState(const SMapOptions&) = 0;
  virtual void		renderState(const SPause&) = 0;
  virtual void		renderState(const SGameOver&) = 0;
  virtual void		renderState(const SScore&) = 0;
  virtual void		renderBegin() = 0;
  virtual void		renderEnd() = 0;
  virtual void		renderTransition() = 0;
  virtual EKey		getEvent() const = 0;

  virtual std::string	getRendererName() const = 0;

  virtual void		init() = 0;
  virtual void		pause() = 0;
  virtual bool		isReady() const = 0;
};

#endif /* !IRENDERER_H_ */

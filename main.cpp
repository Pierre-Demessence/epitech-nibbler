#include	<iostream>
#include	<sstream>
#include	<string>

#include	"GameEngine.hh"
#include	"Map.hh"

static void		usage(std::string filename)
{
  std::cerr << "Usage: " << filename << " mapWidth mapHeight LIB..." << std::endl;
}

static void		error(int errno)
{
  std::string	errormsgs[] =
    {
      "No libary to render game.",
      "Error in library initialization."
    };
  if (errno < 1)
    return ;
  if (errno > 2)
    std::cerr << "Error: " << "Unknown error" << std::endl;
  else
    std::cerr << "Error: " << errormsgs[errno - 1] << std::endl;
}

static int		stoi(std::string str)
{
  std::stringstream	ss;
  int			tmp;

  tmp = 0;
  ss << str;
  ss >> tmp;
  return (tmp);
}

int			main(int ac, char **av)
{
  GameEngine		gm;
  int			res = 0;

  if (ac < 4)
    {
      usage(av[0]);
      return (1);
    }

  Map::defaultWidth = stoi(av[1]);
  if (Map::defaultWidth < MIN_WIDTH)
    Map::defaultWidth = MIN_WIDTH;
  else if (Map::defaultWidth > MAX_WIDTH)
    Map::defaultWidth = MAX_WIDTH;

  Map::defaultHeight = stoi(av[2]);
  if (Map::defaultHeight < MIN_HEIGHT)
    Map::defaultHeight = MIN_HEIGHT;
  else if (Map::defaultHeight > MAX_HEIGHT)
    Map::defaultHeight = MAX_HEIGHT;

  for (int i = 3 ; i < ac ; i++)
    gm.loadLibrary(std::string(av[i]));

  res = gm.run();
  error(res);
  return (res);
}

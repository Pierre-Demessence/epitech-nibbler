#include "AEntity.hh"

//----- ----- Constructors ----- ----- //
AEntity::AEntity()
{
  this->type = AEntity::UNKNOWN;
}

AEntity::AEntity(std::pair<int, int> coords)
{
  this->type = AEntity::UNKNOWN;
  this->addCoordinates(coords);
}

//----- ----- Destructor ----- ----- //
AEntity::~AEntity()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
const std::vector<std::pair<unsigned int, unsigned int> >& AEntity::getCoordinates() const
{
  return (this->coordinates);
}

//----- ----- Setters ----- ----- //
void	AEntity::addCoordinates(std::pair<unsigned int, unsigned int> coords)
{
  this->coordinates.push_back(coords);
}

//----- ----- Setters ----- ----- //
bool	AEntity::isCollide(const AEntity&entity) const
{
  unsigned int max;
  unsigned int max_other;

  max = this->coordinates.size();
  max_other = entity.coordinates.size();
  for (unsigned int i = 0; i < max; ++i)
    {
      for (unsigned int j = 0; j < max_other; ++j)
	{
	  if (this->coordinates[i] == entity.coordinates[j])
	    return (true);
	}
    }
  return (false);
}

AEntity::EEntityType	AEntity::getEntityType() const
{
  return (this->type);
}

#ifndef OPENGLRENDERER_H_
# define OPENGLRENDERER_H_

# include	"IRenderer.hh"
# include	"SMapOptions.hh"
# include	"SHome.hh"
# include	"SCredits.hh"
# include	"SGame.hh"
# include	"SPause.hh"
# include	"SGameOver.hh"
# include	"SScore.hh"
#include	"RendererException.hh"

class		OpenGLRenderer : public IRenderer
{
private:
  bool			ready;
  OpenGLRenderer(const OpenGLRenderer&);
  OpenGLRenderer&	operator=(const OpenGLRenderer&);

  void			drawCase(unsigned int x, unsigned int y,
				 unsigned int mapW, unsigned int mapH) const;
  void			fillCase(unsigned int x, unsigned int y,
				 unsigned int mapW, unsigned int mapH,
				 AEntity*, int num_el) const;

public:
  OpenGLRenderer();
  ~OpenGLRenderer();

  void			init();
  void			pause();
  bool			isReady() const;

  void			renderState(const SHome&);
  void			renderState(const SMapOptions&);
  void			renderState(const SCredits&);
  void			renderState(const SGameOver&);
  void			renderState(const SGame&);
  void			renderState(const SPause&);
  void			renderState(const SScore&);

  std::string		getRendererName() const;
  IRenderer::EKey	getEvent() const;
  void			renderBegin();
  void			renderEnd();
  void			renderTransition();
};

#endif /* !OPENGLRENDERER_H_ */

#include		<algorithm>
#include		<exception>
#include		<csignal>
#include		<GL/freeglut.h>
#include		<unistd.h>
#include		<string.h>
#include		<iostream>
#include		<string>
#include		<sstream>
#include		"OpenGLRenderer.hh"

static int			g_input = 0;

//----- ----- Constructors ----- ----- //
OpenGLRenderer::OpenGLRenderer()
{
  this->ready = 0;
}

//----- ----- Destructor ----- ----- //
OpenGLRenderer::~OpenGLRenderer()
{
  if (glutGetWindow())
    glutDestroyWindow(glutGetWindow());
  glutExit();
}

static void			keyboard(unsigned char key, int x, int y)
{
  (void)x;
  (void)y;
  g_input = key;
}

static void			special(int key, int x, int y)
{
  (void)x;
  (void)y;
  g_input = key;
}

static void			print_txt(const char *txt)
{
  int				cpt;

  cpt = 0;
  while (txt[cpt])
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, txt[cpt++]);
}

IRenderer::EKey			OpenGLRenderer::getEvent() const
{
  g_input = 0;
  glutMainLoopEvent();
  switch (g_input)
    {
    case 0:
      return (IRenderer::NONE);
    case 32:
      return(IRenderer::SPACE);
    case 13:
      return(IRenderer::ENTER);
    case 27:
      return(IRenderer::ESCAPE);
    case GLUT_KEY_UP:
      return(IRenderer::UP);
    case GLUT_KEY_DOWN:
      return(IRenderer::DOWN);
    case GLUT_KEY_RIGHT:
      return(IRenderer::RIGHT);
    case GLUT_KEY_LEFT:
      return(IRenderer::LEFT);
    case GLUT_KEY_PAGE_UP:
      return(IRenderer::PAGE_UP);
    case GLUT_KEY_PAGE_DOWN:
      return(IRenderer::PAGE_DOWN);
    default:
      return(IRenderer::OTHER);
    }
}

std::string			OpenGLRenderer::getRendererName() const
{
  return ("OpenGL");
}

void				OpenGLRenderer::init()
{
  int				argc;
  char				*argv;

  argc = 1;
  argv = NULL;
  glutInit(&argc, &argv);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow("nibbler glut + openGL");
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glutSpecialFunc(special);
  glutKeyboardFunc(keyboard);
  signal(SIGINT, SIG_DFL);
  this->ready = 1;
}

void				OpenGLRenderer::pause()
{
  if (glutGetWindow())
    glutDestroyWindow(glutGetWindow());
  glutExit();
  this->ready = 0;
}

bool				OpenGLRenderer::isReady() const
{
  return (this->ready);
}

void				OpenGLRenderer::renderState(const SHome& state)
{
  std::vector<std::string>	menuEntries = state.getMenuEntries();

  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      if (i == state.getSelectedEntry())
	glColor3d(0.2,0.6,0.2);
      else
	glColor3d(1,1,1);
      glRasterPos2f(0, 0 + (0.1 * menuEntries.size()) - (0.2 * i));
      print_txt(menuEntries[i].c_str());
    }
  glutSwapBuffers();
}

void				OpenGLRenderer::renderState(const SMapOptions& state)
{
  std::vector<std::string>	menuEntries = state.getMenuEntries();

  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      if (i == state.getSelectedEntry())
	glColor3d(0.2,0.6,0.2);
      else
	glColor3d(1,1,1);
      glRasterPos2f(0, 0 + (0.05 * menuEntries.size()) - (0.1 * i));
      print_txt(menuEntries[i].c_str());
    }
  glutSwapBuffers();

}

void				OpenGLRenderer::drawCase(unsigned int x, unsigned int y,
							 unsigned int mapW,
							 unsigned int mapH) const
{
  float				WBlock;
  float				HBlock;

  WBlock = 1.0 / mapW;
  HBlock = 1.0 / mapH;
  glColor3f(0,0,0);
  glBegin(GL_QUADS);

  // lignes horizontales
  glVertex2f(-0.5 + x * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - y * HBlock);

  glVertex2f(-0.5 + x * WBlock, 0.5 - (y + 1) * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - (y + 1) * HBlock);

  // lignes verticales
  glVertex2f(-0.5 + x * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + x * WBlock, 0.5 - (y + 1) * HBlock);

  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - (y + 1) * HBlock);
  glEnd();
}

void				OpenGLRenderer::fillCase(unsigned int x, unsigned int y,
							 unsigned int mapW, unsigned int mapH,
							 AEntity* entity, int num_el) const
{
  float				WBlock;
  float				HBlock;
  AFruit*			fruit;
  AEntity::EEntityType		type = entity->getEntityType();

  WBlock = 1.0 / mapW;
  HBlock = 1.0 / mapH;

  switch (type)
    {
    case AEntity::UNKNOWN:
      glColor3f(255, 255, 255);
      break;
    case AEntity::WALL:
      glColor3f(0.5f, 0.0f, 1.0f);
      break;
    case AEntity::SNAKEPLAYER:
      if (num_el == 0)
	glColor3f(0, 100, 0);
      else
	glColor3f(0.2,0.6,0.2);
      break;
    case AEntity::SNAKEAI:
      if (num_el == 0)
      	glColor3f(25, 25, 112);
      else
	glColor3f(0, 0, 255);
      break;
    case AEntity::FRUIT:
      fruit = static_cast<AFruit*>(entity);
      switch (fruit->getTypeFruit())
	{
	case AFruit::UNKNOWN:
	  glColor3f(255, 255, 255);
	  break;
	case AFruit::APPLE:
	  glColor3f(0, 128, 0);
	  break;
	case AFruit::KIWI:
	  glColor3f(255, 0, 255);
	  break;
	case AFruit::BANANA:
	  glColor3f(255, 255, 0);
	  break;
	case AFruit::TOMATO:
	  glColor3f(1.0f, 0.0f, 0.0f);
	  break;
	}
      break;
    }

  glBegin(GL_QUADS);
  // lignes horizontales
  glVertex2f(-0.5 + x * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - y * HBlock);

  glVertex2f(-0.5 + x * WBlock, 0.5 - (y + 1) * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - (y + 1) * HBlock);

  // lignes verticales
  glVertex2f(-0.5 + x * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + x * WBlock, 0.5 - (y + 1) * HBlock);

  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - y * HBlock);
  glVertex2f(-0.5 + (x + 1) * WBlock, 0.5 - (y + 1) * HBlock);
  glEnd();
}

void				OpenGLRenderer::renderState(const SGame& state)
{
  std::vector<AEntity*>		entities;
  std::vector<ASnake*>		snakes;

  for (unsigned int x = 1 ; x < (state.getMapWidth() - 1) ; x++)
    for (unsigned int y = 1  ; y < (state.getMapHeight() - 1) ; y++)
      this->drawCase(x, y, state.getMapWidth(), state.getMapHeight());

  entities = state.getMapEntities();
  for (unsigned int i = 0 ; i < entities.size() ; i++)
    for (unsigned int j = 0 ; j < entities[i]->getCoordinates().size() ; j++)
      this->fillCase(entities[i]->getCoordinates()[j].first,
  		     entities[i]->getCoordinates()[j].second,
  		     state.getMapWidth(), state.getMapHeight(),
  		     entities[i], 1);

  snakes = state.getSnakes();
  for (unsigned int i = 0 ; i < snakes.size() ; i++)
    for (unsigned int j = 0 ; j < snakes[i]->getCoordinates().size() ; j++)
      this->fillCase(snakes[i]->getCoordinates()[j].first,
  		     snakes[i]->getCoordinates()[j].second,
  		     state.getMapWidth(), state.getMapHeight(),
  		     snakes[i], j);
  glutSwapBuffers();
}

void				OpenGLRenderer::renderState(const SPause& state)
{
  std::string			msg = "PAUSE";

  (void)state;
  glColor3d(1.0,1.0,1.0);
  glRasterPos2f(0, 0);
  print_txt(msg.c_str());
  glutSwapBuffers();
}

void				OpenGLRenderer::renderState(const SCredits& state)
{
  std::string			name1 = "demess_p !";
  std::string			name2 = "taieb_t !";
  std::string			name3 = "rouchy_a !";

  (void)state;
  glRasterPos2f(0, -0.3);
  print_txt(name1.c_str());
  glRasterPos2f(0, 0);
  print_txt(name2.c_str());
  glRasterPos2f(0, 0.3);
  print_txt(name3.c_str());
  glutSwapBuffers();
}

void				OpenGLRenderer::renderState(const SGameOver& state)
{
  std::string			msg = "GAME OVER !";

  (void)state;
  glColor3d(0.2,0.6,0.2);
  glRasterPos2f(0, 0);
  print_txt(msg.c_str());
  glutSwapBuffers();
}

void				OpenGLRenderer::renderState(const SScore& state)
{
  std::stringstream		ss;
  std::vector<ASnake*>		snakes = state.getDeadSnakes();

  for (unsigned int i = 0 ; i < snakes.size() ; i++)
    {
      if (snakes[i]->getEntityType() == AEntity::SNAKEPLAYER)
	glColor3f(0.2,0.6,0.2);
      else
	glColor3f(0, 0, 255);
      glRasterPos2f(0, 0 + (0.05 * snakes.size()) - (0.1 * i));
      ss.str("");
      ss << i+1 << ") Snake : " << snakes[i]->getScore();
      print_txt(ss.str().c_str());
    }
  glutSwapBuffers();
}

void				OpenGLRenderer::renderBegin()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void				OpenGLRenderer::renderEnd()
{
}

void				OpenGLRenderer::renderTransition()
{
  glutSwapBuffers();
}

//----- ----- Functions ----- ----- //
extern "C"
IRenderer*			getInstance()
{
  return (new OpenGLRenderer());
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //

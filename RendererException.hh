#ifndef		RENDEREREXCEPTION_H_
# define	RENDEREREXCEPTION_H_

# include	<exception>
# include	<string>

class		RendererException : public std::exception
{
private:
  std::string	desc;

  		RendererException();
public:
		RendererException(const std::string& desc);
  virtual	~RendererException() throw();
  virtual const char*	what() const throw();
};

#endif /* !RENDEREREXCEPTION_H_ */

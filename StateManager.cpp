#include	"GameEngine.hh"
#include	"StateManager.hh"

//----- ----- Constructors ----- ----- //
StateManager::StateManager()
{
  this->ge = NULL;
}

//----- ----- Destructor ----- ----- //
StateManager::~StateManager()
{
  while (this->states.size() != 0)
    this->popState();
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	StateManager::popState(IRenderer *renderer)
{
  IState*	tmp;

  tmp = this->states.back();
  this->states.pop_back();
  delete tmp;
  if (renderer)
    renderer->renderTransition();
}

void	StateManager::pushState(IState* state, IRenderer *renderer)
{
  this->states.push_back(state);
  if (renderer)
    renderer->renderTransition();
}

int	StateManager::update(IRenderer& renderer)
{
  if (this->states.size() <= 0)
    return (-1);
  this->states.back()->update(renderer, *this);
  return (0);
}

void	StateManager::render(IRenderer& renderer)
{
  unsigned int	begin = 0;

  for (unsigned int i = 0 ; i < this->states.size() ; i++)
    if (!this->states[i]->isTransparent())
      begin = i;
  renderer.renderBegin();
  for (unsigned int i = begin ; i < this->states.size() ; i++)
    this->states[i]->render(renderer);
  renderer.renderEnd();
}

void	StateManager::setGameEngine(GameEngine* ge)
{
  this->ge = ge;
}

void	StateManager::nextRenderer()
{
  this->ge->nextRenderer();
}

void	StateManager::prevRenderer()
{
  this->ge->prevRenderer();
}

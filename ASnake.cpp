#include	<cstdlib>

#include	"ASnake.hh"

const int ASnake::MIN_SPEED = 1;
const int ASnake::MAX_SPEED = 9;

//----- ----- Constructors ----- ----- //
ASnake::ASnake(std::pair<int, int> coords)
  : AEntity(coords)
{
  this->type = AEntity::UNKNOWN;
  this->grownb = 3;
  this->direction = rand() % 3 + 1;
  this->directionNext = this->direction;
  this->fruitsEat = 0;
  this->snakeKill = 0;
  this->mad = 0;
  this->speed = 7;
}

//----- ----- Destructor ----- ----- //
ASnake::~ASnake()
{
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
int	ASnake::getDirection() const
{
  return (this->direction);
}


unsigned int ASnake::getScore() const
{
  return ((this->snakeKill * 100 + this->fruitsEat * 10) + (this->coordinates.size() << 1));
}

void	ASnake::addFruit()
{
  ++(this->fruitsEat);
}

void	ASnake::addKill()
{
  ++(this->snakeKill);
}

unsigned int ASnake::getMad() const
{
  return (this->mad);
}

void	ASnake::addMad(unsigned int timePeriod)
{
  this->mad += timePeriod;
}

bool	ASnake::isCollide(const AEntity& other) const
{
  for (unsigned int i = 0; i < other.getCoordinates().size(); ++i)
    if (this->coordinates[0] == other.getCoordinates()[i])
      return (true);
  return (false);
}

bool	ASnake::isByteHimSelf() const
{
  for (unsigned int i = 1; i < this->coordinates.size(); ++i)
    if (this->coordinates[0] == this->coordinates[i])
      return (true);
  return (false);
}

int	ASnake::getSpeed() const
{
  return (this->speed);
}

//----- ----- Setters ----- ----- //
void	ASnake::setDirection(int direction)
{
  this->direction = direction;
}

void	ASnake::setSpeed(int speed)
{
  if (speed > MAX_SPEED)
    speed = MAX_SPEED;
  if (speed < MIN_SPEED)
    speed = MIN_SPEED;
  this->speed = speed;
}

//----- ----- Methods ----- ----- //
void	ASnake::grow(int nb)
{
  this->grownb += nb;
}

void	ASnake::update(const Map& map, const std::vector<ASnake *>& snakes)
{
  std::pair<int, int> pos;

  this->updateDirection(map, snakes);

  this->direction = this->directionNext;

  pos.first = this->coordinates[0].first;
  pos.second = this->coordinates[0].second;
  switch (direction)
    {
    case 0:
      ++pos.first;
      break;
    case 1:
      --pos.second;
      break;
    case 2:
      --pos.first;
      break;
    case 3:
      ++pos.second;
      break;
    }
  this->coordinates.insert(this->coordinates.begin(), pos);
  if (this->grownb)
    --this->grownb;
  else
    this->coordinates.pop_back();

  if (this->mad)
    --this->mad;
}

void	ASnake::turnLeft()
{

  if (!this->mad)
    {
      this->directionNext = this->direction + 1;
      this->directionNext = (this->directionNext > 3 ? 0 : this->directionNext);
    }
  else
    {
      this->directionNext = this->direction - 1;
      this->directionNext = (this->directionNext < 0 ? 3 : this->directionNext);
    }
}

void	ASnake::turnRight()
{
  if (!this->mad)
    {
      this->directionNext = this->direction - 1;
      this->directionNext = (this->directionNext < 0 ? 3 : this->directionNext);
    }
  else
    {
      this->directionNext = this->direction + 1;
      this->directionNext = (this->directionNext > 3 ? 0 : this->directionNext);
    }
}

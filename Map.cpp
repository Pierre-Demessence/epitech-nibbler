#include	"Map.hh"
#include	"Wall.hh"

unsigned int	Map::defaultWidth = 42;
unsigned int	Map::defaultHeight = 42;

//----- ----- Constructors ----- ----- //
Map::Map()
{
  this->setWidth(Map::defaultWidth);
  this->setHeight(Map::defaultHeight);
  this->addWallBorder();
}

//----- ----- Destructor ----- ----- //
Map::~Map()
{
  AEntity *tmp;

  while (this->entities.size() != 0)
    {
      tmp = this->entities.back();
      this->entities.pop_back();
      delete tmp;
    }
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
unsigned int	Map::getWidth() const
{
  return (this->width);
}

unsigned int	Map::getHeight() const
{
  return (this->height);
}

void	Map::setSnakes(const std::vector<ASnake*> &allSnakesAlive)
{
  this->snakes = allSnakesAlive;
}

bool	Map::isCaseSnakesFree(const std::pair<unsigned int, unsigned int>& pair) const
{
  for (unsigned int i = 0; i < this->snakes.size(); ++i)
    {
      for (unsigned int j = 0; j < this->snakes[i]->getCoordinates().size(); ++j)
	{
	  if (this->snakes[i]->getCoordinates()[j] == pair)
	    return (false);
	}
    }
  return (true);
}

void	Map::deleteEntity(unsigned int pos)
{
  if (pos > this->entities.size())
    return ;
  this->entities.erase(this->entities.begin() + pos);
}

const std::vector<AEntity*>&	Map::getEntities() const
{
  return (this->entities);
}

//----- ----- Setters ----- ----- //
void	Map::setWidth(unsigned int width)
{
  this->width = width;
  if (this->width < MIN_WIDTH)
    this->width = MIN_WIDTH;
  else if (this->width > MAX_WIDTH)
    this->width = MAX_WIDTH;
}

void	Map::setHeight(unsigned int height)
{
  this->height = height;
  if (this->height < MIN_HEIGHT)
    this->height = MIN_HEIGHT;
  else if (this->height > MAX_HEIGHT)
    this->height = MAX_HEIGHT;
}

void	Map::addEntity(AEntity* e)
{
  this->entities.push_back(e);
}

//----- ----- Methods ----- ----- //
void	Map::addWallBorder()
{
  for (unsigned int x = 0 ; x < this->width ; x++)
    {
      for (unsigned int y = 0 ; y < this->height ; y++)
	{
	  if (x == 0 || y == 0 || x == this->width - 1 || y == this->height - 1)
	    this->addEntity(new Wall(std::pair<int, int>(x, y)));
	}
    }
}

bool	Map::isCaseGood(const std::pair<unsigned int, unsigned int>& cmp) const
{
  for (unsigned int i = 0 ; i < this->entities.size() ; i++)
    {
      if (this->entities[i]->getEntityType() != AEntity::FRUIT)
	for (unsigned int j = 0; j < this->entities[i]->getCoordinates().size(); ++j)
	  {
	    if (cmp == this->entities[i]->getCoordinates()[j])
	      return (false);
	  }
    }
  return (true);
}

bool	Map::isCaseFree(const std::pair<unsigned int, unsigned int>& cmp) const
{
  for (unsigned int i = 0 ; i < this->entities.size() ; i++)
    {
      for (unsigned int j = 0; j < this->entities[i]->getCoordinates().size(); ++j)
	{
	  if (cmp == this->entities[i]->getCoordinates()[j])
	    return (false);
	}
    }
  return (true);
}

unsigned int	Map::getNbFruit() const
{
  unsigned int res = 0;

  for (unsigned int i = 0 ; i < this->entities.size() ; i++)
    {
      if (this->entities[i]->getEntityType() == AEntity::FRUIT)
	++res;
    }
  return (res);
}

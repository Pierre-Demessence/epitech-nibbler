#ifndef SPAUSE_H_
# define SPAUSE_H_

# include	"IState.hh"

class		SPause : public IState
{
private:
  SPause(const SPause&);
  SPause&	operator=(const SPause&);

public:
  SPause();
  ~SPause();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  bool	isTransparent() const;
};

#endif /* !SPAUSE_H_ */

#include	<algorithm>
#include	<exception>
#include	<SDL/SDL_gfxPrimitives.h>
#include	<signal.h>
#include	<sstream>

#include	"RendererException.hh"
#include	"SDLRenderer.hh"

//----- ----- Constructors ----- ----- //
SDLRenderer::SDLRenderer()
{
  if (TTF_Init() != 0)
    throw RendererException("Unable to init TTF.");
  this->font = TTF_OpenFont(FONTPATH, FONTSIZE);
  if (!this->font)
    throw RendererException("Unable to load font.");
  this->ready = 0;
}

//----- ----- Destructor ----- ----- //
SDLRenderer::~SDLRenderer()
{
  TTF_CloseFont(this->font);
  TTF_Quit();
}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
bool	SDLRenderer::isReady() const
{
  return (this->ready);
}

//----- ----- Setters ----- ----- //
//----- ----- Methods ----- ----- //
void	SDLRenderer::clearScreen()
{
  SDL_FillRect(this->screen, NULL, SDL_MapRGB(this->screen->format, 12, 42, 42));
}

void	SDLRenderer::writeText(const std::string& text, int x, int y, int color, int fontStyle)
{
  SDL_Surface*	text_sur;
  SDL_Rect	pos;
  SDL_Color	col;

  pos.x = x;
  pos.y = y;
  col.r = (color & 0x00FF0000) >> 16;
  col.g = (color & 0x0000FF00) >> 8;
  col.b = (color & 0x000000FF);
  TTF_SetFontStyle(this->font, fontStyle);
  text_sur = TTF_RenderUTF8_Solid(this->font, text.c_str(), col);
  SDL_BlitSurface(text_sur, NULL, this->screen, &pos);

  TTF_SetFontStyle(this->font, TTF_STYLE_NORMAL);
  SDL_FreeSurface(text_sur);
}

void	SDLRenderer::writeTextCenter(const std::string& text,
				     int color, int fontStyle,
				     int xoffset, int yoffset)
{
  SDL_Rect	dstr;
  int		textw = 0;
  int		texth = 0;

  TTF_SizeText(this->font, text.c_str(), &textw, &texth);
  dstr.x = (WIN_WIDTH - textw) / 2 + xoffset;
  dstr.y = (WIN_HEIGHT - texth) / 2 + yoffset;
  this->writeText(text, dstr.x, dstr.y, color, fontStyle);
}

unsigned int	SDLRenderer::getColor(AFruit::EFruitType e) const
{
  switch (e)
    {
    case AFruit::APPLE:
      return (0x16FF16FF);
    case AFruit::KIWI:
      return (0x00FFAAFF);
    case AFruit::BANANA:
      return (0xFFFF00FF);
    case AFruit::TOMATO:
      return (0xFF5555FF);
    default:
      return (0x000000FF);
    }
}

unsigned int	SDLRenderer::getColor(AEntity::EEntityType e) const
{
  switch (e)
    {
    case AEntity::SNAKEPLAYER:
      return (0x42FF42FF);
    case AEntity::SNAKEAI:
      return (0xAAAAFFFF);
    case AEntity::WALL:
      return (0xFF9966FF);
    default:
      return (0x000000FF);
    }
}

unsigned int	SDLRenderer::getColor(AEntity* e) const
{
  if (e->getEntityType() == AEntity::FRUIT)
    return (this->getColor(static_cast<AFruit*>(e)->getTypeFruit()));
  else
    return (this->getColor(e->getEntityType()));
}

void	SDLRenderer::fillCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH, AEntity* entity)
{
  unsigned int	color;

  (void)entity;
  color = this->getColor(entity);
  this->fillCase(x, y, mapW, mapH, color);
}

void	SDLRenderer::fillCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH, unsigned int color)
{
  int	xoffset, yoffset;

  xoffset = ((WIN_WIDTH - mapW * CASE_WIDTH) / 2);
  yoffset = ((WIN_HEIGHT - mapH * CASE_HEIGHT) / 2);

  boxColor(this->screen,
	   xoffset + x * CASE_WIDTH + 1,
	   yoffset + y * CASE_HEIGHT + 1,
	   xoffset + x * CASE_WIDTH + CASE_WIDTH - 1,
	   yoffset + y * CASE_HEIGHT + CASE_HEIGHT - 1,
	   color);
}

void	SDLRenderer::drawCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH)
{
  int	xoffset, yoffset;

  xoffset = ((WIN_WIDTH - mapW * CASE_WIDTH) / 2);
  yoffset = ((WIN_HEIGHT - mapH * CASE_HEIGHT) / 2);

  rectangleRGBA(this->screen,
		xoffset + x * CASE_WIDTH,
		yoffset + y * CASE_HEIGHT,
		xoffset + x * CASE_WIDTH + CASE_WIDTH,
		yoffset + y * CASE_HEIGHT + CASE_HEIGHT,
		255, 255, 255, 255);
}

void	SDLRenderer::renderState(const SHome& state)
{
  std::vector<std::string>	menuEntries = state.getMenuEntries();
  SDL_Rect	dstr;
  int		textw = 0;
  int		texth = 0;

  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      TTF_SizeText(this->font, menuEntries[i].c_str(), &textw, &texth);
      dstr.x = (WIN_WIDTH - textw) / 2;
      dstr.y = ((WIN_HEIGHT - texth) / 2) - (menuEntries.size() * texth / 2) + ((texth * 1.25) * i);
      this->writeText(menuEntries[i], dstr.x, dstr.y, 0x00FFFFFF);
      if (i == state.getSelectedEntry())
	this->writeText(menuEntries[i], dstr.x, dstr.y, 0x00FFFFFF, TTF_STYLE_UNDERLINE);
    }
}

void	SDLRenderer::renderState(const SCredits&)
{
  std::vector<std::string>	people;
  SDL_Rect	dstr;
  int		textw = 0;
  int		texth = 0;

  people.push_back("Credits:");
  people.push_back("rouchy_a");
  people.push_back("taieb_t ");
  people.push_back("demess_p");
  for (unsigned int i = 0 ; i < people.size() ; i++)
    {
      TTF_SizeText(this->font, people[i].c_str(), &textw, &texth);
      dstr.x = (WIN_WIDTH - textw) / 2;
      dstr.y = ((WIN_HEIGHT - texth) / 2) - (people.size() * texth / 2) + ((texth * 1.25) * i);
      this->writeText(people[i], dstr.x, dstr.y, 0x00FFFFFF);
    }

  for (int i = 0 ; i < 100 ; i++)
    {
      int	x = rand() % WIN_WIDTH / CASE_WIDTH;
      int	y = rand() % WIN_HEIGHT / CASE_HEIGHT;

      boxColor(this->screen,
	       x * CASE_WIDTH,
	       y * CASE_HEIGHT,
	       x * CASE_WIDTH + CASE_WIDTH,
	       y * CASE_HEIGHT + CASE_HEIGHT,
	       rand() % 0xFFFFFFFF);
    }
}

std::string	SDLRenderer::getRendererName() const
{
  return ("SDL");
}

unsigned int	SDLRenderer::shadeColor(unsigned int color, int percent) const
{
  unsigned int	amt = round(2.55f * percent);
  int	r = (color >> 24 & 0xFF) + amt;
  int	g = (color >> 16 & 0xFF) + amt;
  int	b = (color >> 8  & 0xFF) + amt;

  r = (r <= 255 ? r : 255);
  r = (r >= 0   ? r : 0);
  g = (g <= 255 ? g : 255);
  g = (g >= 0   ? g : 0);
  b = (b <= 255 ? b : 255);
  b = (b >= 0   ? b : 0);

  color = 0;
  color += r << 24;
  color += g << 16;
  color += b << 8;
  color += 0xFF;

  return (color);
}

void	SDLRenderer::renderState(const SGame& state)
{
  std::vector<AEntity*>	entities;
  std::vector<ASnake*>	snakes;
  unsigned int		color;

  entities = state.getMapEntities();
  for (unsigned int i = 0 ; i < entities.size() ; i++)
    for (unsigned int j = 0 ; j < entities[i]->getCoordinates().size() ; j++)
      this->fillCase(entities[i]->getCoordinates()[j].first, entities[i]->getCoordinates()[j].second, state.getMapWidth(), state.getMapHeight(), entities[i]);

  snakes = state.getSnakes();
  for (unsigned int i = 0 ; i < snakes.size() ; i++)
    for (unsigned int j = 0 ; j < snakes[i]->getCoordinates().size() ; j++)
      {
	if (j == 0)
	  color = this->shadeColor(this->getColor(snakes[i]->getEntityType()), -42);
	else
	  color = this->shadeColor(this->getColor(snakes[i]->getEntityType()), j * -2);
	this->fillCase(snakes[i]->getCoordinates()[j].first, snakes[i]->getCoordinates()[j].second, state.getMapWidth(), state.getMapHeight(), color);
      }
}

void	SDLRenderer::renderState(const SMapOptions& state)
{
  std::vector<std::string>	menuEntries = state.getMenuEntries();
  SDL_Rect	dstr;
  int		textw = 0;
  int		texth = 0;

  this->writeTextCenter("Map Options :", 0x00FFFFFF, TTF_STYLE_BOLD, 0, - WIN_HEIGHT / 3);
  for (unsigned int i = 0 ; i < menuEntries.size() ; i++)
    {
      TTF_SizeText(this->font, menuEntries[i].c_str(), &textw, &texth);
      dstr.x = (WIN_WIDTH - textw) / 2;
      dstr.y = ((WIN_HEIGHT - texth) / 2) - (menuEntries.size() * texth / 2) + ((texth * 1.25) * i);
      this->writeText(menuEntries[i], dstr.x, dstr.y, 0x00FFFFFF);
      if (i == state.getSelectedEntry())
	this->writeText(menuEntries[i], dstr.x, dstr.y, 0x00FFFFFF, TTF_STYLE_UNDERLINE);
    }
}

void	SDLRenderer::renderState(const SPause&)
{
  boxRGBA(this->screen, 0, 0, WIN_WIDTH, WIN_HEIGHT, 127, 127, 127, 50);
  this->writeTextCenter("Game Paused", 0x00FFFFFF);
}

void	SDLRenderer::renderState(const SGameOver&)
{
  boxRGBA(this->screen, 0, 0, WIN_WIDTH, WIN_HEIGHT, 127, 127, 127, 50);
  this->writeTextCenter("Game over", 0x00FFFFFF);
}

void	SDLRenderer::renderState(const SScore& state)
{
  std::stringstream	ss;

  this->writeTextCenter("Scores :", 0x00FFFFFF, TTF_STYLE_BOLD, 0, - WIN_HEIGHT / 4);
  std::vector<ASnake*>	snakes = state.getDeadSnakes();
  for (unsigned int i = 0 ; i < snakes.size() ; i++)
    {
      ss.str("");
      ss << i+1 << ") Snake" <<
	(snakes[i]->getEntityType() == AEntity::SNAKEPLAYER ? "Player" : "AI")
	 <<" : " << snakes[i]->getScore();
      this->writeTextCenter(ss.str(), (snakes[i]->getEntityType() == AEntity::SNAKEPLAYER ? 0x0000FF00 : 0x005555FF), TTF_STYLE_NORMAL, 0, -200 + (i + 1) * 50);
    }
}

void	SDLRenderer::renderBegin()
{
  this->clearScreen();
}

void	SDLRenderer::renderEnd()
{
  SDL_Flip(this->screen);
}

void	SDLRenderer::renderTransition()
{
  this->clearScreen();
}

IRenderer::EKey		SDLRenderer::getEvent() const
{
  SDL_Event		event;

  if (SDL_PollEvent(&event) && event.type == SDL_KEYDOWN)
    {
      switch (event.key.keysym.sym)
	{
	case SDLK_LEFT:
	  return (IRenderer::LEFT);
	case SDLK_RIGHT:
	  return (IRenderer::RIGHT);
	case SDLK_UP:
	  return (IRenderer::UP);
	case SDLK_DOWN:
	  return (IRenderer::DOWN);
	case SDLK_SPACE:
	  return (IRenderer::SPACE);
	case SDLK_RETURN:
	  return (IRenderer::ENTER);
	case SDLK_ESCAPE:
	  return (IRenderer::ESCAPE);
	case SDLK_PAGEUP:
	  return (IRenderer::PAGE_UP);
	case SDLK_PAGEDOWN:
	  return (IRenderer::PAGE_DOWN);
	default:
	  return (IRenderer::OTHER);
	}
    }
  return (IRenderer::NONE);
}

void	SDLRenderer::init()
{
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
    return ;
  this->screen = SDL_SetVideoMode(WIN_WIDTH, WIN_HEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
  if (!this->screen)
    return ;
  SDL_WM_SetCaption("Nibbler SDL", "Nibbler SDL");
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  (void)signal(SIGINT, SIG_DFL);
  this->ready = 1;
}

void	SDLRenderer::pause()
{
  SDL_Quit();
  this->ready = 0;
}

//----- ----- Functions ----- ----- //
extern "C"
IRenderer*		getInstance()
{
  return (new SDLRenderer());
}

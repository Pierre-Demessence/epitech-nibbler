#ifndef SDLRENDERER_H_
# define SDLRENDERER_H_

# include	<SDL/SDL.h>
# include	<SDL/SDL_ttf.h>

# include	"IRenderer.hh"
# include	"SHome.hh"
# include	"SCredits.hh"
# include	"SMapOptions.hh"
# include	"SGame.hh"
# include	"SPause.hh"
# include	"SGameOver.hh"
# include	"SScore.hh"

# define	FONTPATH	"libSDL/resources/dejavusans.ttf"
# define	FONTSIZE	42

# define	CASE_WIDTH	15
# define	CASE_HEIGHT	15

class		SDLRenderer : public IRenderer
{
private:

  SDL_Surface*	screen;
  TTF_Font*	font;
  bool		ready;

  SDLRenderer(const SDLRenderer&);
  SDLRenderer&	operator=(const SDLRenderer&);

  void		clearScreen();
  void		writeText(const std::string& text, int x, int y,
			  int color, int fontStyle = TTF_STYLE_NORMAL);
  void		writeTextCenter(const std::string& text,
				int color, int fontStyle = TTF_STYLE_NORMAL,
				int xoffset = 0, int yoffset = 0);
  void		drawCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH);
  void		fillCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH, AEntity*);
  void		fillCase(unsigned int x, unsigned int y, unsigned int mapW, unsigned int mapH, unsigned int color);
  unsigned int	getColor(AFruit::EFruitType) const;
  unsigned int	getColor(AEntity::EEntityType) const;
  unsigned int	getColor(AEntity*) const;
  unsigned int	shadeColor(unsigned int color, int percent) const;

public:
  SDLRenderer();
  ~SDLRenderer();

  void		renderState(const SHome&);
  void		renderState(const SCredits&);
  void		renderState(const SGame&);
  void		renderState(const SMapOptions&);
  void		renderState(const SPause&);
  void		renderState(const SGameOver&);
  void		renderState(const SScore&);
  void		renderBegin();
  void		renderEnd();
  void		renderTransition();
  EKey		getEvent() const;

  std::string	getRendererName() const;

  void		init();
  void		pause();
  bool		isReady() const;
};

#endif /* !SDLRENDERER_H_ */

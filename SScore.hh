#ifndef		SSCORE_H_
# define	SSCORE_H_

# include	"IState.hh"
# include	"ASnake.hh"

class		SScore : public IState
{
private:
  SScore(const SScore&);
  SScore&	operator=(const SScore&);
  std::vector<ASnake*> deadSnakes;

public:
  SScore(const std::vector<ASnake*>&);
  ~SScore();

  void	update(IRenderer&, StateManager&);
  void	render(IRenderer&) const;
  const std::vector<ASnake *> & getDeadSnakes() const;
  bool	isTransparent() const;
};

#endif /* !SSCORE_H_ */

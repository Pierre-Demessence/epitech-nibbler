
#include <cstdlib>
#include "FruitFactory.hh"

//----- ----- Constructors ----- ----- //
FruitFactory::FruitFactory()
{}

//----- ----- Destructor ----- ----- //
FruitFactory::~FruitFactory()
{}

//----- ----- Operators ----- ----- //
//----- ----- Getters ----- ----- //
//----- ----- Setters ----- ----- //
void	FruitFactory::addFruit(int prob, AFruit * fruit)
{
  this->fruits.push_back(std::pair<int, AFruit *>(prob, fruit));
}

AFruit 	*FruitFactory::pickFruit() const
{
  int	randomInt;
  int	sum = 0;

  for (unsigned int i = 0; i < this->fruits.size(); ++i)
    sum += this->fruits[i].first;
  if (sum == 0)
    return (NULL);
  randomInt = rand() % sum;
  sum = 0;
  for (unsigned int i = 0; i < this->fruits.size(); ++i)
    {
      sum += this->fruits[i].first;
      if (randomInt < sum)
	return (this->fruits[i].second->clone());
    }
  return (NULL);
}
